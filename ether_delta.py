"""
Created on Sep 17, 2017

@author: dak
"""
import json
import requests
import cloudflare_grappa
#import cfscrape
import hashlib
import random
from requests import RequestException
from wsio import wsio
from aio_cf_session import aio_cf_session
from time import sleep
import sys
try:
	from urlparse import urlparse
except ImportError:
	from urllib.parse import urlparse

def print_scrapage(scraper):
	attrs = ["auth", "cert", "cookies", "headers", "hooks", "params", "proxies", "data"]
	for attr in attrs:
		print('attrs: '+attr)
		val = getattr(scraper, attr, None)
		if val:
			try:
				for i in val:
					print (str(i)+":: "+str(val[i]))
			except:
				print(val)
	cj = scraper.cookies.get_dict()
	for i in cj:
		print ("cfscrape cookie " + str(i)+":: "+str(cj[i]))
		


class EtherDelta(object):
	""" ribbon and bow around the etherdelta.com api """

	@staticmethod
	def getAPINonce():
		return hashlib.sha256(str(random.random())[2:].encode()).hexdigest()
	
	def fetch(self, url):
		"""
		basic synchronous http request on given url
		"""
		try:
			resp = self.cfscraper.get(url)
			if resp.status_code == 200 and resp.text:
				return resp.json()
			else:
				return None
		except RequestException as e:
			if hasattr(e, 'message'):
				print(e.message)
			else:
				print('Exception: '+str(e))
			return None
	
	async def get(self, url):
		"""
		basic asyncio http request on given url
		"""
		print("asynchronous get on "+url)
		if not self.http_worker or not self.aioSession:
			print("no valid async looper or session")
			return False
		async with self.aioSession.get(url) as resp:
			if resp.status != 200:
				raise Exception("Bad response status from EtherDelta: "+str(resp.status))
			return await resp.json()
			
	async def getReturnTicker(self):
		"""
		 ${self.config.apiServer}/nonce/${self.APINonce}/returnTicker`,
		 "returnTicker", callback)
		"""
		if not self.apiInitted:
			raise Exception("API not initialized in getReturnTicker")
		return await self.get(self.apiServer + "/nonce/" + self.apiNonce + "/returnTicker")
		
	def fetchReturnTicker(self):
		"""
		 https://api.etherdelta.com/returnTicker
		 this particular call is synchronous, and is mainly for an idea of trade landscape in the setup
		"""
		return self.fetch("https://api.etherdelta.com/returnTicker")
		
	async def getFunds(self, token=None):	
		"""
		${self.config.apiServer}/nonce/${self.APINonce}/funds/${self.addrs[self.selectedAccount]}/${self.selectedToken.addr}/0`,
		 "funds", callback, self.transformFunds)

		"""
		if not self.apiInitted:
			raise Exception("API not initialized in getFunds")
		if not token:
			token = self.currentToken
		return await self.get(self.apiServer + "/nonce/" + self.apiNonce + "/funds/" +\
							self.addrs[self.selectedAccount] + "/" + self.tokenAddrs[token] + "/0")
		
	async def getTrades(self, token=None, addr=None):
		"""
		${self.config.apiServer}/nonce/${self.APINonce}/trades/${self.selectedToken.addr}/0`,
		"trades", callback, self.transformTrades)
		"""
		if not self.apiInitted:
			raise Exception("API not initialized in getTrades")
		tokenAddress = None
		if addr:
			tokenAddress = addr
		else:
			if not token:
				token = self.currentToken
			if not token in self.tokenAddrs:
				raise Exception("Token " + token + " has no address in getTrades")
			tokenAddress = self.tokenAddrs[token]
		return await self.get(self.apiServer + "/nonce/" + self.apiNonce + "/trades/" + tokenAddress + "/0")
		
	def fetchTrades(self, token=None):
		"""
		${self.config.apiServer}/nonce/${self.APINonce}/trades/${self.selectedToken.addr}/0`,
		"trades", callback, self.transformTrades)
		"""
		if not self.apiInitted:
			raise Exception("API not initialized in getTrades")
		if not token:
			token = self.currentToken
		if not token in self.tokenAddrs:
			raise Exception("Token " + token + " has no address in getTrades")
		tokenAddress = self.tokenAddrs[token]
		return self.fetch(self.apiServer + "/nonce/" + self.apiNonce + "/trades/" + tokenAddress + "/0")
		
	async def getMyTrades(self, token=None):
		"""
	   ${self.config.apiServer}/nonce/${self.APINonce}/myTrades/${self.addrs[self.selectedAccount]}/${self.selectedToken.addr}/0`,
	    "myTrades", callback, self.transformTrades)
		"""
		if not self.apiInitted:
			raise Exception("API not initialized in getMyTrades")
		if not token:
			token = self.currentToken
		return await self.get(self.apiServer + "/nonce/" + self.apiNonce + "/myTrades/" +
							self.addrs[self.selectedAccount] + "/" + self.tokenAddrs[token] + "/0")
		
	async def getOrders(self, token=None):
		"""
		${self.config.apiServer}/nonce/${self.APINonce}/orders/${self.selectedToken.addr}/0`,
		"trades", callback, self.transformTrades)
		"""
		if not self.apiInitted:
			raise Exception("API not initialized in getOrders")
		if not token:
			token = self.currentToken
		return await self.get(self.apiServer + "/nonce/" + self.apiNonce + "/orders/" + self.tokenAddrs[token] + "/0")
	
	def fetchOrders(self, token=None):
		"""
		${self.config.apiServer}/nonce/${self.APINonce}/orders/${self.selectedToken.addr}/0`,
		"trades", callback, self.transformTrades)
		"""
		if not self.apiInitted:
			raise Exception("API not initialized in getOrders")
		if not token:
			token = self.currentToken
		return self.fetch(self.apiServer + "/nonce/" + self.apiNonce + "/orders/" + self.tokenAddrs[token] + "/0")

	async def getMyOrders(self, token=None):
		"""
	   ${self.config.apiServer}/nonce/${self.APINonce}/myOrders/${self.addrs[self.selectedAccount]}/${self.selectedToken.addr}/0`
	   , "myOrders", callback, self.transformOrders)
		"""
		if not self.apiInitted:
			raise Exception("API not initialized in getMyOrders")
		if not token:
			token = self.currentToken
		return await self.get(self.apiServer + "/nonce/" + self.apiNonce + "/myOrders/" +\
							self.addrs[self.selectedAccount] + "/" + self.tokenAddrs[token] + "/0")


	def configFromJson(self, data):
		print("reading config json data")
		if "apiServer" in data:
			self.apiServer = data["apiServer"]
			if (type(self.apiServer) is list):
				self.apiServer = self.apiServer[0]
		else:
			print("api defaulting to api.etherdelta.com")
			self.apiServer = 'https://api.etherdelta.com'
		
		if "socketServer" in data:
			self.socketServer = data["socketServer"]
		else:
			print("socketServer not found")
			return False
		
		if "ethProvider" in data:
			self.ethProvider = data["ethProvider"]
		else:
			print("ethProvider not found")
			return False
		
		if "ethProvider" in data:
			self.ethProvider = data["ethProvider"]
		else:
			print("ethProvider not found")
			return False
		
		if "etherscanAPIKey" in data:
			self.apiKey = data["etherscanAPIKey"]
		else:
			print("etherscanAPIKey not found")
			return False
		
		if "contractEtherDeltaAddrs" in data:
			self.contractEtherDeltaAddrs = data["contractEtherDeltaAddrs"]
			if type(self.contractEtherDeltaAddrs) is list and "addr" in self.contractEtherDeltaAddrs[0]:
				self.contractEtherDeltaAddrs = self.contractEtherDeltaAddrs[0]["addr"]
		else:
			print("contractEtherDeltaAddrs not found")
			return False

		if "userCookie" in data:
			self.userCookie = data["userCookie"]
		else:
			self.userCookie = "EtherDelta"
		if "eventsCacheCookie" in data:
			self.eventsCacheCookie = data["eventsCacheCookie"]
		else:
			self.eventsCacheCookie = "EtherDelta_eventsCache"
		if "ordersCacheCookie" in data:
			self.ordersCacheCookie = data["ordersCacheCookie"]
		else:
			self.ordersCacheCookie = "EtherDelta_ordersCache"

		if "tokens" in data:
			for item in data["tokens"]:
				if not ("name" in item) or not ("addr" in item) or not ("decimals" in item):
					print("malformed token item not found " +str(item))
					return False
				name = item["name"]
				addr = item["addr"]
				decimals = item["decimals"]
				self.tokenAddrs[name] = addr
				self.tokenDecimals[name] = decimals
				self.addrName[addr] = name
		else:
			print("tokens not found")
			return False
		if "ethGasPrice" in data:
			self.ethGasPrice = (data["ethGasPrice"])
		if "gasApprove" in data:
			self.gasApprove = (data["gasApprove"])
		if "gasDeposit" in data:
			self.gasDeposit = (data["gasDeposit"])
		if "gasWithdraw" in data:
			self.gasWithdraw = (data["gasWithdraw"])
		if "gasTrade" in data:
			self.gasTrade = (data["gasTrade"])
		if "gasOrder" in data:
			self.gasOrder = (data["gasOrder"])
		if "minOrderSize" in data:
			self.minOrderSize = (data["minOrderSize"])
		if "defaultPair" in data:
			d = data["defaultPair"]
			if "token" in d:
				self.currentToken = d["token"]
			if "base" in d:
				self.currentBase = d["base"]
		self.apiInitted = True
		return True
	
	def startAsyncio(self, http_worker):
		"""
		setup async components of the ether delta socket comms, also ensuring the existence of the basic config file
		"""
		self.http_worker = http_worker
		while self.cfscraper is None:
			made_first_cnx = self.get_ed_config(self.config_url)
			if not made_first_cnx:
				print('error making initial connection, trying again in '+str(self.initial_cnx_timeout)+ ' secs')
				sleep(self.initial_cnx_timeout)
		print("creating aiosession...")
#		self.aioSession = CloudflareScraper(loop=self.http_worker)	
		self.aioSession = aio_cf_session(self.cfscraper, self.domain, loop=self.http_worker)
		print("just some dumb test thing ...")
		self.socket = list()
		if (type(self.socketServer) is list):
			for sockUrl in self.socketServer:
				s = wsio(self.aioSession, sockUrl)# "https://socket.etherdelta.com")#self.socketServer)
				print("creating socket on "+s.url)
				self.socket.append(s)
		else:
			s = wsio(self.aioSession, self.socket)
			print("creating socket on "+s.url)
			self.socket.append()
		
	def close(self):
		""" closes the session to free up resources. We should be able to restart from this, but it will need a new aioSession. """
		if self.aioSession:
			self.aioSession.close()
		self.aioSession = None
	
	def get_ed_config(self, config_url):
		try:
			print("getting EtherDelta config from "+config_url+"...", file=sys.stderr)
			cfscraper = cloudflare_grappa.create_scraper()
				
			resp = cfscraper.get(config_url)
			print_scrapage(cfscraper)
			print("processed scrapage")
			if len(resp.text) == 0:
				print("empty response from ether delta")
			else:
				print("etherdelta response length "+str(len(resp.text)))
				if self.configFromJson(json.loads(resp.text)):
					print("EtherDelta object successfully set up...", file=sys.stderr)
					self.domain = urlparse(resp.url).netloc
					self.cfscraper = cfscraper
					return True
				else:
					print("EtherDelta not initialized ...", file=sys.stderr)				
		except requests.exceptions.RequestException as e:
			print(e)
#			raise e
			return False
		except ValueError as e:
			print(e)
#			raise e
			return False
		return False

	def __init__(self, _http_worker=None, site_url_base='https://etherdelta.com', config_path='config/main.json'):
		""" construct EtherDelta() given a  """
		print("initializing EtherDelta...", file=sys.stderr)
		random.seed()
		self.http_worker = _http_worker
		self.apiNonce = EtherDelta.getAPINonce()
		self.apiInitted = False
		self.tokenAddrs = dict()
		self.tokenDecimals = dict()
		self.addrName = dict()
		self.apiKey = ""
		self.apiServer = ""
		self.ethGasPrice = 4000000000
		self.gasApprove = 25000
		self.gasDeposit = 25000
		self.gasWithdraw = 25000
		self.gasTrade = 25000
		self.gasOrder = 25000
		self.minOrderSize = 0.001
		self.currentToken = "MTH"
		self.currentBase = "ETH"
		self.aioSession = None
		self.socketServer = None
		self.socket = None
		self.domain = None
		self.cfscraper = None
		self.config_url = site_url_base+'/'+config_path
		self.initial_cnx_timeout = 30
		self.get_ed_config(self.config_url)

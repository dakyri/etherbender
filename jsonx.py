'''
Created on Oct 30, 2017

@author: dak
'''

from datetime import datetime

def jsonDumpable(obj, string_int=False):
	if isinstance(obj, Exception):
		return str(obj)
	if isinstance(obj, datetime):
		return str(obj.isoformat())
	if string_int and isinstance(obj, int):
		return str(obj) # handle infinite precision ints ok on the other side
	if isinstance(obj, dict):
		data = {}
		for key, value in obj.items():
			data[key] = jsonDumpable(value, string_int)
		return data
	if hasattr(obj, "__dict__"):
		data = {}
		for key, value in obj.__dict__.items():
			data[key] = jsonDumpable(value, string_int)
		return data
	if isinstance(obj, list) or isinstance(obj, tuple):
		data = []
		for i in obj:
			data.append(jsonDumpable(i, string_int))
		return data
	return obj

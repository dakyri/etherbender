'''
Created on Oct 8, 2017

@author: dak
'''

import aiohttp

def aio_cf_session(scraper, domain, **kwargs):	
	cookie_domain = None
	
	for d in scraper.cookies.list_domains():
		print(d)
		if d.startswith(".") and d in ("." + domain):
			cookie_domain = d
			break
	else:
		raise ValueError("Unable to find Cloudflare cookies. Does the site actually have Cloudflare IUAM (\"I'm Under Attack Mode\") enabled?")
	
	cf_tokens = {
		"__cfduid": scraper.cookies.get("__cfduid", "", domain=cookie_domain),
		"cf_clearance": scraper.cookies.get("cf_clearance", "", domain=cookie_domain)
		}
	print(str(cf_tokens))
	cf_headers = {"User-Agent": scraper.headers["User-Agent"]}
	cs = aiohttp.ClientSession(cookies=cf_tokens, headers=cf_headers, **kwargs)
	return cs
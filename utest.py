'''
Created on Oct 27, 2017

@author: dak
'''
from worker import JoDolgozo
from worker import toOrderJsonStr
import json
from datetime import datetime
import queue

worker = JoDolgozo()
with open('tests/order-0-COB.json') as f:
	o0 = json.load(f)
with open('tests/order-1-COB.json') as f:
	o1 = json.load(f)
with open('tests/trades-1-COB.json') as f:
	t1 = json.load(f)
	
def mockTrades(stuff):
	return t1

inf, deals = worker.analyseOrdersResponse(o0, 'COB')

print("########## inf 0 ###################################")
print("token " + inf.token)
print("time " + str(inf.time))
print("startTime " + str(inf.startTime))
print("active " + str(inf.active))
print("nBuys " + str(inf.nBuys))
print("nSells " + str(inf.nSells))
print("aveBuyPrice " + str(inf.aveBuyPrice))
print("minBuyPrice " + str(inf.minBuyPrice))
print("maxBuyPrice " + str(inf.maxBuyPrice))
print("aveSellPrice " + str(inf.aveSellPrice))
print("minSellPrice " + str(inf.minSellPrice))
print("maxSellPrice " + str(inf.maxSellPrice))
print("sweets " + str(len(inf.sweets)))
for i in inf.sweets:
	print(" * " + str(i))
	print(" ** " + str(inf.sweets[i]))

	
inf1, deals1 = worker.analyseOrdersResponse(o1, 'COB')

print("########## inf 1 ###################################")
print("token " + inf1.token)
print("time " + str(inf1.time))
print("startTime " + str(inf1.startTime))
print("active " + str(inf1.active))
print("nBuys " + str(inf1.nBuys))
print("nSells " + str(inf1.nSells))
print("aveBuyPrice " + str(inf1.aveBuyPrice))
print("minBuyPrice " + str(inf1.minBuyPrice))
print("maxBuyPrice " + str(inf1.maxBuyPrice))
print("aveSellPrice " + str(inf1.aveSellPrice))
print("minSellPrice " + str(inf1.minSellPrice))
print("maxSellPrice " + str(inf1.maxSellPrice))
print("sweets " + str(len(inf1.sweets)))
for i in inf1.sweets:
	print(" * " + str(i))
	print(" ** " + str(inf1.sweets[i]))
	
c = inf.sweetsNotIn(deals1)
print("changed sweets between frames " + str(len(c)))
for i in c:
	print(" * " + str(i))

analysisQ = queue.Queue()
for d in c:
	analysisQ.put(d)

maxSecsLookingForTrades = 600

print("quee is " + str(analysisQ.qsize()))

while analysisQ.qsize() > 0:
	deal = analysisQ.get()
	# do some analysis here. find out more about this deal if we can!!!
	print("annaLiza looking at "+str(deal))
	trades = worker.findTrade("sell", deal.tokenGet, deal.price, buyer=deal.user)
	print(trades)
	if trades:
		print(toOrderJsonStr(deal)) # json msg put q going out to web pages
	else:
		qt = datetime.now() - deal.qTimestamp
		if (qt.total_seconds() < maxSecsLookingForTrades):
			print('annaliza return deal to queue')
			analysisQ.put(deal) # put this deal back in the queue
			print('getting mock trade')
			trade_response = mockTrades(deal.tokenGet)
			worker.trades[deal.tokenGet] = worker.processTradesResponse(trade_response)





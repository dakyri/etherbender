'''
Created on Oct 6, 2017

@author: dak
'''

from ether_delta import EtherDelta
from threading import Thread
import json
import asyncio
import websockets
from datetime import datetime
from datetime import timedelta
import traceback
import sys
import iso8601
from rydrs import lintr
from jsonx import jsonDumpable
import collections
import math
from unicodedata import bidirectional
from aiohttp import WSServerHandshakeError
from aiohttp import ClientResponseError
from web3_trade import w3w
from aiohttp.client_exceptions import ClientConnectionError
from exchange import Order, Trade

madLogging = False
madLog = None
def log(level, *args):
	if level >= 2:
		if madLogging:
			print(*args, file=madLog)
	if level <= 2:
		print(*args)


class SequencedSioEvent:
	def __str__(self):
		return "event "+self.event + " @ " + str(self.time)
	
	def __init__(self, t, event, socket=None, params={}, slamda=None, *slamda_params):
		self.time = t
		self.socket = socket
		self.event = event
		self.params = params
		self.slamda = slamda
		self.slamda_params = slamda_params
		
class EventSequencer:
	"""
	not really striving for thread safety, just coroutine safety ;)
	"""
	def __str__(self):
		return "Sequencer "+str([str(x) for x in self.evQ])
	
	def find(self, socket, evt, params):
		try:
			return next(event for event in self.evQ if (event.event==evt and params == event.params and (socket is None or event.socket == socket)))
		except StopIteration:
			return None
	
	def addUniquely(self, t_rel, socket, ev, params={}, slamda=None, *slamda_params):
		"""
		add something uniquely to the sequencer
		@param t_rel: relative time in seconds untill the event
		@param socket: io device for the event
		@param ev the: scheduled request command
		@param params: an object of parameters for that request
		@param slambda: a function to call on sequencing
		@param slambda_params: parameters for that function
		"""
		if not self.find(socket, ev, params):
			self.add(t_rel, socket, ev, params, slamda, *slamda_params)

	def add(self, t_rel, socket, ev, params={}, slamda=None, *slamda_params):
		"""
		add something to the sequencer
		@param t_rel: relative time in seconds until the event
		@param socket: io device for the event
		@param ev the: scheduled request command
		@param params: an object of parameters for that request
		@param slambda: a function to call on sequencing
		@param slambda_params: parameters for that function
		"""
		now = (datetime.now() - self.startTime).total_seconds()
		at_t = now + t_rel
		ev = SequencedSioEvent(at_t, ev, socket, params, slamda, *slamda_params)
		ins_at = 0
		last_t = sys.float_info.min
		for e in self.evQ:
			if at_t >= last_t and at_t <= e.time:
				break;
			last_t = e.time 
			ins_at += 1
		self.evQ.insert(ins_at, ev)
		if self.waitNextTask:
			self.waitNextTask.cancel()
		self.addedToQ.set()
	
	def getTopOfQ(self):
		try:
			head = self.evQ.popleft()
		except IndexError:
			return None
		if callable(head.slamda):
			if isinstance(head.slamda_params,  collections.Iterable):
				head.slamda(*head.slamda_params)
			elif head.slamda_params is None:
				head.slamda()
			else:
				head.slamda(head.slamda_params)
		return head
	
	async def next(self):
		while True:
			if len(self.evQ) == 0:
				log(1, 'q is still empty. setting addedToQ')
				self.addedToQ.clear()
				await self.addedToQ.wait()
			head = self.evQ[0]
			now = (datetime.now() - self.startTime).total_seconds()
			log(1, str(self))
			delay = head.time - now
			if delay <= 0: # head is the next and it is due
				return self.getTopOfQ()
			try:
#				log(1, 'about to sleep for '+str(delay))
				self.waitNextTask = asyncio.ensure_future(asyncio.sleep(delay, loop=self.loop))
				await self.waitNextTask
				self.waitNextTask = None
				return self.getTopOfQ()
			except asyncio.CancelledError:
				pass

	def start(self):
		self.startTime = datetime.now()		
	
	def __init__(self, loop):
		self.loop = loop
		self.evQ = collections.deque()
		self.startTime = datetime.now()
		self.addedToQ = asyncio.Event(loop=loop)
		self.waitNextTask = None

class TokenInfo:
	def __str__(self):
		return "Token " + str(self.name) + ' ' + str(self.addr)+ ", quoteVolume " + str(self.quoteVolume) + ", baseVolume " + str(self.baseVolume)\
			+ ", last " + str(self.last) + ", percentChange " + str(self.percentChange)
			
	def baCenter(self):
		return (self.bid + self.ask)/2 if self.bid and self.ask else (self.bid if self.bid else self.ask)
	
	def __init__(self, name='', addr='', status="inactive", quoteVolume=0.0, baseVolume=0.0, last=0.0, percentChange=0.0, bid = 0.0, ask=0.0):
		self.name = name
		self.addr = addr
		self.status = status
		self.quoteVolume = quoteVolume
		self.baseVolume = baseVolume
		self.last = last
		self.percentChange = percentChange
		self.bid = bid
		self.ask = ask

class MarketInfo:
	def __str__(self):
		return "MarketInfo @ "+str(self.time)
	
	def merge(self, inf):
		if inf:
			if self.time < inf.time:
				self.time = inf.time
			else:
				self.startTime = inf.time
				
			if inf.minBuyPrice < self.minBuyPrice:
				self.minBuyPrice = inf.minBuyPrice
			elif inf.maxBuyPrice > self.maxBuyPrice:
				self.maxBuyPrice = inf.maxBuyPrice
			if inf.minSellPrice < self.minSellPrice:
				self.minSellPrice = inf.minSellPrice
			elif inf.maxSellPrice > self.maxSellPrice:
				self.maxSellPrice = inf.maxSellPrice
		else:
			pass

	def addSweet(self, oi):
		self.sweets[oi.oid] = oi
		
	def findSweet(self, oid):
		if oid in self.sweets:
			return self.sweets[oid]
		return None
	
	def delSweet(self, oid):
		if oid in self.sweets:
			del self.sweets[oid]
		return None
			
	def __init__(self, token):
		self.token = token
		self.time = datetime.now()
		self.startTime = self.time
		self.nBuys = 0
		self.nSells = 0
		self.aveBuyPrice = 0
		self.minBuyPrice = sys.float_info.max
		self.maxBuyPrice = 0
		self.aveSellPrice = 0
		self.minSellPrice = sys.float_info.max
		self.maxSellPrice = 0
		self.totalBuyPrice = 0
		self.totalSellPrice = 0
		self.center = 0
		self.sweets = dict()

def toTokenInfoJsonStr(obj):
	if not isinstance(obj, list):
		raise Exception("Expected a list of TokenInfo object to convert to message")
	return json.dumps({"tokens": jsonDumpable(obj) })
	
def toXInfoJsonStr(obj):
	if not isinstance(obj, MarketInfo):
		raise Exception("Expected an MarketInfo object to convert to message")
	return json.dumps({"xinfo": jsonDumpable(obj) })
	
def toOrderJsonStr(obj):
	if not isinstance(obj, Order):
		raise Exception("Expected an Order to convert to message")
	return json.dumps({"order": jsonDumpable(obj)})	
		
def toTradeJsonStr(obj):
	if not isinstance(obj, Trade):
		raise Exception("Expected a Trade to convert to message")
	return json.dumps({"trade": jsonDumpable(obj)})	
		
def toErrJsonStr(e):
	return json.dumps({"err": jsonDumpable(e)})
		
class JoDolgozo:
	sugarMagic = 3
	maxSecsLookingForTrades = 2000
	pingTime = 25
	baseMarketReqTime = 60
	reconnectTime = 60 # seconds to attempt a reconnection after a failed connection
	maxConnectRetries = 10
		
	@staticmethod
	def runEventLoop(loop, server):
		log(1, "start event loop")
		asyncio.set_event_loop(loop)
		log(1, "init'd q runner...")
		loop.run_until_complete(server)
		log(1, "got q runner...")
		loop.run_forever();
		
	def getTokenInfo(self, tokAdr):
		if not tokAdr in self.tokensAdrIdx:
			mi = TokenInfo(addr=tokAdr)
			self.tokens.append(mi)
			self.tokensAdrIdx[tokAdr] = mi
		return self.tokensAdrIdx[tokAdr]

	def getMarketInfo(self, tok):
		if not tok in self.market:
			mi = MarketInfo(tok)
			if tok in self.tokensAdrIdx:
				tlt = self.tokensAdrIdx[tok]
				mi.center = tlt.baCenter()
			self.market[tok] = mi
		return self.market[tok]

	def getDeals(self, tok):
		if not tok in self.deals:
			self.deals[tok] = dict()
		return self.deals[tok]
	
	def getTrades(self, tokAddr):
		if not tokAddr in self.trades:
			self.trades[tokAddr] = dict()
		return self.trades[tokAddr]
	
	def delDeal(self, deal=None, oid=None, tok=None):
		if oid is None:
			if deal is None:
				return False
			oid = deal.oid
		if tok is None:
			if deal is None:
				return False
			tok = deal.token()
		if tok in self.deals:
			deals = self.deals[tok]
			if oid in deals:
				del deals[oid]
				return True
		return False
		
	def delTrade(self, t):
		tok = t.tokenAddr
		if tok in self.trades:
			trades = self.trades[tok]
			if t.txHash in trades:
				del trades[t.txHash]
				return True
		return False
				
	def processReturnTicker(self, tickerJson):
		"""
		handle the etherdelta return ticker json. builds up tokenInfo as it goes along
		 @return: a set of the tokens it found
		"""
		tokens_found = set();
		for i in tickerJson:
			dta = tickerJson[i]
			addr = dta['tokenAddr']
			if (addr in self.edelta.addrName):
				nm = self.edelta.addrName[addr]
			else:
				nm = i
			quoteVolume = dta['quoteVolume']
			baseVolume = dta['baseVolume']
			last = dta['last']
			percentChange = dta['percentChange']
			bid = dta['bid']
			ask = dta['ask']
			t = self.getTokenInfo(addr)
			t.name = nm;
#			t.status = 'inactive'
			t.quoteVolume = quoteVolume
			t.baseVolume = baseVolume
			t.last = last
			t.percentChange = percentChange
			t.bid = bid
			t.ask = ask
			if addr in self.market:
				mi = self.market[addr]
				mi.center = t.baCenter()
			tokens_found.add(nm)

		log(1, "found "+str(len(tokens_found))+ " tokens in returnTicker response")
		return tokens_found
		
	def processTradesResponse(self, tradesJson):
		"""
		this is used in two expected ways. either the tradesJson are a full market dump on a token, or an incremental covering any tokens
		 @return traded_toks: set containing tokens in buy orders
		"""
		traded_toks = set()
		cnt = 0
		total_cnt = 0
		if tradesJson:
			for i in tradesJson:
				txHash = i.get("txHash")
				tokenAddr = i.get("tokenAddr")
				price_s = i.get("price")
				amount_s = i.get("amount")
				base_s = i.get("amountBase")
				buyer = i.get("buyer")
				seller = i.get("seller")
				side = i.get("side")
				date_s = i.get("date")
				if not (txHash is None or tokenAddr is None or price_s is None or amount_s is None or base_s is None or buyer is None or seller is None or side is None or date_s is None):
					price = float(price_s)
					amount = lintr(amount_s)
					base = lintr(base_s)
					total_cnt += 1
					date = iso8601.parse_date(date_s)
					t = Trade(txHash=txHash, side=side, tokenAddr=tokenAddr, price=price, amt=amount, base=base, buy=buyer, sell=seller, timestamp=date)
					trades = self.getTrades(tokenAddr)
					if txHash not in trades:
						log(3, str(datetime.now()) + " => " + str(t))
						cnt += 1
						trades[txHash] = t
						traded_toks.add(tokenAddr)
				else:
					log(1, "incomplete trade response "+ str(i))
			log(1, "got "+str(len(traded_toks))+" traded tokens " + str(cnt) + "/" + str(total_cnt) + " unique trades in response")
		else:
			log(1, "empty trades response")
		return traded_toks

	def checkAddDeal(self, deals, o):
		"""
		add order to the deals object, indexed
		@returns o: the order, or the original if we already have one on this id
		@returns dup: if this is a duplicate of something we already have
		"""
		dup = False
		if o.oid in deals:
			oo = deals[o.oid]
			if o.availableVolume == oo.availableVolume:
				dup = True
			else:
				if o.availableVolume < oo.availableVolume:
					oo.availableVolume = o.availableVolume
			o = oo
		else:
			o.qTimestamp = datetime.now()
			deals[o.oid] = o
		return o, dup
	
	def cleanupTradesCache(self, tok):
		log(1, "*** placeholder for purgin cached trades on "+tok)

	def analyseOrdersResponse(self, ordersJson, src_name, full_market_token=None):
		"""
		this is used in two expected ways. either the ordersJson are a full market dump on a token, or an incremental covering any tokens
		 @return buy_tok: set containing tokens in buy orders
		 @return sell_tok: set containing tokens in sell orders
		 @return new_deals: is a list of newly appearing sweet deals
		 @return changed_deals: is a list of sweet deals that have been deleted or sold volume
		"""
		buy_toks = set()
		sell_toks = set()
		new_deals = []
		changed_deals = []
		if full_market_token:
			inf = self.getMarketInfo(full_market_token)
			inf.nBuys = 0
			inf.nSells = 0

		if ordersJson:
			if "buys" in ordersJson:
				log(1, "got " +str(len(ordersJson["buys"]))+ " buys")

				for b in ordersJson["buys"]:
					tgt = b["tokenGet"]
					deals = self.getDeals(tgt)
					inf = self.getMarketInfo(tgt)
					buy_toks.add(tgt)
					oid = b["id"]
					price = float(b["price"])
					deleted = False
					if "deleted" in b:
						deleted = b["deleted"]
					if deleted:
						# or maybe we don't remove this from consideration in stats?
						# what does 'deleted' exactly mean in this context
						# is it distinct from cancelled?
						inf.nBuys -= 1
						inf.totalBuyPrice -= price
						
						od = inf.findSweet(oid)
						if od:
							inf.delSweet(oid)
							changed_deals.append(od)
						if self.delDeal(oid=oid, tok=tgt):
							log(1, 'deleted deal '+oid)
						else:
							log(1, 'failed to delete deal '+oid)
					else:
						# and likewise here, because a deal could end up here from a change, so
						# isn't an interesting this to average out like that
						inf.totalBuyPrice += price
						inf.nBuys += 1
						
						if price < inf.minBuyPrice:
							inf.minBuyPrice = price
						elif price > inf.maxBuyPrice:
							inf.maxBuyPrice = price
							
						user = b["user"]
						nons = int(b["nonce"])
						tgv = b["tokenGive"]
						agt = lintr(b["amountGet"])
						agv = lintr(b["amountGive"])
						avol = lintr(b["availableVolume"])
						exp = int(b["expires"])
						v = b["v"]
						r = b["r"]
						s = b["s"]
						updated = b["updated"]
						
# TODO FIXME reconsider the order of calculations and searching for duplicates now that we're spamming sockets
						o = Order(oid=oid, side="buy", price=price, user=user, aget=agt, tget=tgt, agiv=agv, tgiv=tgv, avol=avol,
								 	exp=exp, nons=nons, timestamp=updated, v=v, r=r, s=s)
						o, duplicateOrder = self.checkAddDeal(deals, o)
						now = datetime.now()
						if not duplicateOrder:
							log(3, str(now) + " @ " + src_name + " => " + str(o))
# don't set it in 'deals' until we've checked the list of sweets so we can spot changes
#							log(1, "maybe "+str(price) + " is above average, relative to "+str(inf.center))
							tok_name = (self.edelta.addrName[inf.token] if inf.token in self.edelta.addrName else str(inf.token))
							is_sweet_deal = False
							if self.shit_token_filter and self.shit_token_filter(inf):
								log(2, "*** " + str(now) + " this is a bullshit token and we won't touch it " + tok_name)
							else:
								if inf.center > 0:
									log(1, "*** inf.center for "+tok_name+" is "+str(inf.center))
									is_sweet_deal = (price > inf.center * self.sugarMagic)
									if is_sweet_deal:
										if self.sweet_filter and not self.sweet_filter(price):
											log(2, "*** rejected by the sweetness filter")
											is_sweet_deal = False						
								else:
									if self.sweet_filter:
										log(1, "*** centerless token ", tok_name, " calculating sweet filter ")
										is_sweet_deal = self.sweet_filter(price)
									
							if is_sweet_deal:
								od = inf.findSweet(oid)
								if od: # assume that if we find an aold sweet deal, it must already be in the full deals
									if od.availableVolume != avol:
										if avol < od.availableVolume:
											log(1, 'found an updated sweet deal ' + oid)
											od.availableVolume = avol
											changed_deals.append(od)
									else:
										log(1, 'ignoring previously found deal ' + oid + ' that seems unchanged' )
								else: # newly sweet
									log(2, "*** " + str(now) + " found a newly sweet deal on "+tok_name+" by generalized sweetness "
											+ str(inf.center * self.sugarMagic) + " vs " + str(price)+" vol "+str(o.availableVolume))
# THIS IS IT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
									if self.viableOrder(o):
										try:
											log(1, "actually trying it")
											amount_to_take = o.availableVolume
											do_gastimate = True
											tx, gastimate = self.w3.takeEDOrder(self.wallet, o, amountGive_wei=amount_to_take, gasPrice_gwei=self.xpressGasPrice_gwei,
																do_testTrade=True, do_gasEstimate=do_gastimate)
											if self.wallet:
												if tx:
													log(2, "** holllly fuck. we got a transaction receipt "+str(tx))
													o.description += "Tried to take with "+str(tx)
													o.status = "TRIED"													
												else:
													log(2, "** tried but got no trancsaction reciept")
													o.description += "Take attempt failed"+str(tx)
													o.status = "LAME"													
											else:
												o.description += "Would have tried for, but no valid wallet"
												o.status = "LAME"
											if do_gastimate:
												log(2, "gas estimate: "+gastimate)
										except Exception as e:
											log(2, "got an error going for it... at least we tried hey: "+str(e))
											o.description += "Tried to take but failed "+str(e)
											o.status = "LAME"
											traceback.print_exc(file=sys.stdout)
										then = now
										now = datetime.now()
										log(2, "found at", str(then), "now", str(now), "block", self.w3.getBlockNumber())										
									else:
										log(2, "Sweet deal is not viable maybe fuck hell who know but i won't right now")
										o.description += "Deal is not viable"
										o.status = "LAME"

									new_deals.append(o)
									inf.addSweet(o)
							if inf.center <= 0:
								# either this token is never accounted before, or had inadequate information. now we at least should have some
								# so we should request the return ticker again		
								if (now - self.lastMarketRecheck).total_seconds() > self.marketCheckGap_secs:
									log(1, "*** token "+tgt+" has no center! Maybe it's new!!! Requesting a fresh look at the ticker list.")
									self.reloadTickerList = True
									self.reqQ.addUniquely(30, self.edelta.socket[0], 'getMarket')
									self.lastMarketRecheck = now
						else:
							log(2, str(now), "=>", "skipping duplicate order", o.oid ,"on", src_name)

				for bt in buy_toks:
					inf = self.getMarketInfo(bt)
					if inf.nBuys > 0:
						inf.aveBuyPrice = inf.totalBuyPrice / inf.nBuys
					else:
						inf.aveBuyPrice = 0
			if "sells" in ordersJson:
				log(1, "got " +str(len(ordersJson["sells"]))+ " sells")
				
				for s in ordersJson["sells"]:
					price = float(s["price"])
					tgv = s["tokenGive"]
					deleted = False
					inf = self.getMarketInfo(tgv)
					sell_toks.add(tgv)
					if "deleted" in s:
						deleted = s["deleted"]
					if deleted:
						# see notes above about stats
						inf.totalSellPrice -= price
						inf.nSells -= 1
					else:
						# see notes above about stats
						inf.totalSellPrice += price
						inf.nSells += 1
						if price < inf.minSellPrice:
							inf.minSellPrice = price
						elif price > inf.maxSellPrice:
							inf.maxSellPrice = price
				for st in sell_toks:
					inf = self.getMarketInfo(st)
					if inf.nSells > 0:
						inf.aveSellPrice = inf.totalSellPrice / inf.nSells
					else:
						inf.nSells = 0
		else:
			log(1, "got a blank response")
		log(1, str(len(new_deals))+ " new deals of interest and "+str(len(changed_deals))+" changed deals")
		return (buy_toks, sell_toks, new_deals, changed_deals)
	
	def orderActualVolume(self, addr, vol):
		d = None
		if addr in self.edelta.addrName:
			name = self.edelta.addrName[addr]
			if name in self.edelta.tokenDecimals:
				d = self.edelta.tokenDecimals[name]
		if d is None:
			name, symbol, d = self.w3.getERC20Info(addr)
			self.edelta.tokenAddrs[symbol] = addr
			self.edelta.addrName[addr] = symbol
			self.edelta.tokenDecimals[symbol] = d
			log(1, "Added " + symbol + ", " + str(d) + " to mappings")
		while d > 0:
			vol = vol / 10.0
			d -= 1
		return vol
	
	def viableOrder(self, o):
		'''
		it's sweet but is there enough in this order to be worth bidding for
		'''
		gain = o.price * self.orderActualVolume(o.token(), o.availableVolume)
		if self.maximumViableSDGain_eth > 0 and gain > self.maximumViableSDGain_eth:
			return False
		return gain >= self.minimumViableSDGain_eth
	
	async def processOrdersToken(self, order_response, src_name, token):
		"""
		Analysis and processing for full update of a single token's market.
		This is the version of the call used for the 'orders' component of the new api 'market' respons
		"""
		try:
			log(1, "requesting orders on "+token)			
			buy_toks, sell_toks, newDeals, changedDeals  = self.analyseOrdersResponse(order_response, src_name, full_market_token=True)
			log(1, str(len(newDeals)) + " new hot deals in processTokenOrders")
			inf = self.getMarket(buy_toks[0]) if len(buy_toks) > 1 else None
			if inf:
				await self.msgQ.put(toXInfoJsonStr(inf))		# json msg put q going out to web pages
				
# these have already gone out with the xinfo packets			
				sQGen = [self.analysisQ.put(d) for d in newDeals] # check the new deals in the analysis q anyway. maybe the change gets missed
				if sQGen:	
					await asyncio.wait(sQGen)
					
				aQGen = [self.analysisQ.put(d) for d in changedDeals]	# object list message going out to analyser
				if aQGen:
					await asyncio.wait(aQGen)
					
		except Exception as e:
			await self.msgQ.put(toErrJsonStr(e))
			traceback.print_exc(file=sys.stdout)

	async def processOrdersDelta(self, orders, socket_name):
		"""
		Analysis and processing for incremental updates of the market. At the moment, that will cover a range of tokens.
		"""
		try:
			log(1, "processing orders list")			
			buy_toks, sell_toks, newDeals, changedDeals = self.analyseOrdersResponse(orders, socket_name) # inf, deals = 
			
			for bt in buy_toks:
				inf = self.market[bt]
				await self.msgQ.put(toXInfoJsonStr(inf))		# json msg put q going out to web pages

# these have already gone out with the xinfo packets			
#			sQGen = [self.msgQ.put(toOrderJsonStr(d)) for d in newDeals] # json msg put q going out to web pages
			sQGen = [self.analysisQ.put(d) for d in newDeals] # check the new deals in the analysis q anyway. maybe the change gets missed
			if sQGen:	
				await asyncio.wait(sQGen)
				
			aQGen = [self.analysisQ.put(d) for d in changedDeals]	# object list message going out to analyser
			if aQGen:
				await asyncio.wait(aQGen)
		except Exception as e:
			await self.msgQ.put(toErrJsonStr(e))
			traceback.print_exc(file=sys.stdout)
			
	async def socketConnect(self, socket):
		"""
		open the given socket
		@param socket: is a wsio structure wrapping the socket io over websocket operations
		@return: true if succeed, else false
		"""
		count = 0
		while count < self.maxConnectRetries:
			try:
				await socket.connect()
				log(1, "retry attempt "+str(count)+" seems to succeed ...")
				return True
			except WSServerHandshakeError as e:
				log(1, "socketConnect() Web socket connect handshake error, "+e.message)
				log(1, "\tcode is "+str(e.code))
				log(1, "\theaders "+str(e.headers))
				log(1, "\tscheduling reconnection in "+str(self.reconnectTime)+" seconds")
			except ClientResponseError as e:
				log(1, "socketConnect() Aiohttp client response error error, "+e.message)
				log(1, "\tcode is "+str(e.code))
				log(1, "\theaders "+str(e.headers))
				log(1, "\tscheduling reconnection in "+str(self.reconnectTime)+" seconds")
			except ClientConnectionError as e:
				log(1, "socketConnect() ClientConnectionError, "+str(e))
				log(1, "\tscheduling reconnection in "+str(self.reconnectTime)+" seconds")
			except Exception as e:
				log(1, "socketConnect() unexpected exception, "+str(e))
				raise e
			await asyncio.sleep(self.reconnectTime)
			count += 1
		return False
		
	async def nefariousTrawlerOfGutterSpanningEmissions(self, socket):
		"""
		Main task receiving and processing messages from ether delta.
		@param socket: is a wsio structure wrapping the socket io over websocket operations
		"""
		socket_name = socket.url[7:17]
		await self.socketConnect(socket)
		log(1, "connected websocket ok to", socket_name)
		while True:
			msg = await socket.rx()
			if msg.event == 'orders':
				log(1, '<< orders from', socket_name)
				await self.processOrdersDelta(msg.data, socket_name)
			elif msg.event == 'trades':
				log(1, '<< trades from', socket_name)
				trades_received = self.processTradesResponse(msg.data)
				map(self.cleanupTradesCache, trades_received)
				self.renalyzeNewTrades.set()
			elif msg.event == 'funds':
				log(1, '<< funds from', socket_name)
			elif msg.event == 'market':
				log(1, '<< market from', socket_name)
				r = msg.data
				if "orders" in r:
					orders_response = r["orders"]
					tok = None
					try:
						tok = orders_response["buys"][0]["tokenGet"]
					except (KeyError, IndexError):
						try:
							tok = orders_response["sells"][0]["tokenGive"]
						except (KeyError, IndexError):
							pass
					if tok:
						await self.processOrdersToken(orders_response, socket_name, tok)
					else:
						log(1, "no orders in market")
				if "trades" in r:
					trades_received = self.processTradesResponse(r["trades"])
					map(self.cleanupTradesCache, trades_received)
					self.renalyzeNewTrades.set()
				if "returnTicker" in r:
					if self.reloadTickerList or not self.hasInitialTickerList: # rebuild the token list in the recieved order, which is significant
						self.tokens = []
						self.tokensAdrIdx = {}
						tokens_in_ticker = self.processReturnTicker(r["returnTicker"])
						self.hasInitialTickerList = True
						self.reloadTickerList = False
						self.lastMarketRecheck = datetime.now()
						await self.msgQ.put(toTokenInfoJsonStr(self.tokens))		# json msg put q going out to web pages
					else:
						log(1, "return ticker already set")
				else:
					log(1, "no return ticker in market")
					log(1, str(r))
					if not self.hasInitialTickerList:
						log(1, "rescheduling initial getMarket")
						self.reqQ.add(5, self.edelta.socket[0], 'getMarket')
					elif self.reloadTickerList:
						log(1, "rescheduling reloading getMarket")
						self.reqQ.add(30, self.edelta.socket[0], 'getMarket')
			elif msg.event == 'open':
				log(1, '<< open on', socket_name)
				self.pingTime = math.floor(0.9 * socket.pingInterval / 1000.0)
				log(1, "conservative ping time in seconds = "+str(self.pingTime))
				self.wsReady.set()
			elif msg.event == 'on':
				log(1, '<< close from', socket_name)
			elif msg.event == 'error':
				log(1, '<< error on ', socket_name, " ", str(msg.data))
				await self.msgQ.put(toErrJsonStr(msg.data))
			elif msg.event == 'cnx':
				log(1, '<< connection to', socket_name)
			elif msg.event == 'discnx':
				log(1, '<< disconnection from', socket_name)
			elif msg.event == 'ping':
				log(1, '<< ping from', socket_name)
			elif msg.event == 'pong':
				log(1, '<< pong ', socket_name)
			elif msg.event == 'closed':
				log(1, '<< closed ', socket_name)
				log(1, '... attempting reconnect ... ')
				await self.socketConnect(socket)
			else:
				log(1, '<< surprising event:'+msg.event, "from", socket_name)
		
	async def infernalEngineOfHiddenDesires(self):
		"""Main task handling message requests going to ether delta."""	
		await self.wsReady.wait()
		self.reqQ.add(0, self.edelta.socket[0], 'getMarket')
		# for this one we don't really need the slamda_params, but that does give us a broader scope for calculations for the slamda
		pinger = lambda pingt, socket: self.reqQ.add(pingt, socket, 'ping', {}, lambda pt, socket: pinger(pt, socket), pingt, socket)
		for s in self.edelta.socket:
			pinger(self.pingTime, s)

		while True:
			event = await self.reqQ.next()
			if event.socket.ws is None: # reschedule
				log(1, "!! ", event.event, "to", event.socket.url[7:17], " rescheduling")
				self.reqQ.add(10, event.socket, event.event, event.params, event.slamda, event.slamda_params)
			elif event.event:
				log(1, ">>", event.event, "to", event.socket.url[7:17])
				await event.socket.tx(event.event, event.params)

	
	async def tranceMitter(self, websocket, path):
		"""Grab messages from the various websocket queues and send them to their destinations """
		log(1, "connect to tranceMitter")
		wsq = asyncio.Queue(loop=self.workerLoop)
		self.cueBall.add(wsq)
		try:
			# send any initial setup messages
			if (self.hasInitialTickerList):
				await websocket.send(toTokenInfoJsonStr(self.tokens))
			while True:
				msg = await wsq.get()
				log(1, "sending message ... "+msg[0:30]+" ...")
				await websocket.send(msg)
		finally:
			self.cueBall.remove(wsq)
			
	async def trancePlitter(self):
		"""Multiplex messages from the main message q and send to all the websocket queues """
		while True:
			msg = await self.msgQ.get()
			end_points = [q.put(msg) for q in self.cueBall]
			if end_points:
				await asyncio.wait(end_points)
				
	async def annaLiza(self, edelta):
		""" hunt around for more details on deals that have passed """
		while True:
			deal = await self.analysisQ.get()
			# do some analysis here. find out more about this deal if we can!!!
			log(1, "annaLiza looking at "+str(deal))
			traded = self.findTrade("sell", deal.tokenGet, deal.price, buyer=deal.user)
			if traded: # deal will already have been sent, but we have details
				deal.tTimestamp = datetime.now()
				matchedTrade = False
				checkMissingOrderUpdate = False
				foundVolume = 0
				volumeAlreadyTraded = deal.amountGet - deal.availableVolume
				for ti in traded:
					if ti.orderId is None:
						log(1, "found traded "+str(ti))
						if ti.amount < volumeAlreadyTraded - foundVolume:
							pass # request more info
							ti.orderId = deal.oid
							ti.status += "-found"
							log(1, toTradeJsonStr(ti))
							matchedTrade = True
						else:
							log(1, "maybe missed an update on this order?")
							checkMissingOrderUpdate = True
					elif ti.orderId == deal.oid:
						foundVolume += ti.amount
				if matchedTrade:
					deal.status += "-found"
					await self.msgQ.put(toOrderJsonStr(deal))
					sQGen = [self.msgQ.put(toTradeJsonStr(d)) for d in traded if d.orderId == deal.oid] # json msg put q going out to web pages
					if sQGen:	
						await asyncio.wait(sQGen)
				else:
					log(1, "Deal nearly matched but not quite -> returned to analysis queue")
					self.renalysisQ.append(deal)
				if checkMissingOrderUpdate:
					self.reqQ.addUniquely(30, self.edelta.socket[0], 'getMarket', { 'token': deal.tokentGet, 'user': deal.user }) 
			else:
				if deal.qTimestamp:
					qt = datetime.now() - deal.qTimestamp
				else:
					qt = timedelta(0)
				if (qt.total_seconds() < self.maxSecsLookingForTrades):
					log(1, "Deal not matched but still not around for too long -> returned to analysis queue")
					self.renalysisQ.append(deal)
# TODO maybe add a request for all the market on this token to get all the current trades
# OTH trades come through eventually and new ones will update. chances of a noticed deal coming through without
# an associated trade coming eventually are low. 
				else: # probably deleted or somehow we've missed it
					log(1, "Deal too long in analysis queue")
					deal.status += "-deleted"
					deal.description += ", Deal too long in analysis queue ("+str(qt.total_seconds())+" secs)"
					await self.msgQ.put(toOrderJsonStr(deal)) 
# how the fuck did this slip through, attached to the while loop?
#		else:
#			self.renalysisQ.append()
				
	async def renaLiza(self):
		"""
		pop anything whose analysis has been deferred back into the analysis Q proper
		"""
		while True:
			await self.renalyzeNewTrades.wait()
			self.renalyzeNewTrades.clear()
			toRenalyze = self.renalysisQ.copy() # work on a shallow copy. if our wait blocks, self.renalysisQ may change and that's a bit messy 
			self.renalysisQ.clear()
			added = set()
			for d in toRenalyze:
				if d.oid not in added:
					log(1, 'requeueing for analysis '+d.oid)
					added.add(d.oid)
					await self.analysisQ.put(d) # XXX we may need to be careful about doubles ending in analysisQ ????
				else:
					log(1, 'not double requeueing '+d.oid)
					
	def start(self):
		"""Start the main event loop and worker thread """
		log(1, "about to create loop...")
		self.workerLoop = asyncio.new_event_loop()
		log(1, "building queues for the event loop ...")
		self.cueBall = set() # all the ws q's together having a party
		self.reqQ = EventSequencer(loop=self.workerLoop) # a sequenced queue of requests to be sent along socket
		self.msgQ = asyncio.Queue(loop=self.workerLoop)
		self.wsReady = asyncio.Event(loop=self.workerLoop)
		self.analysisQ = asyncio.Queue(loop=self.workerLoop)
		self.renalysisQ = collections.deque()
		self.renalyzeNewTrades = asyncio.Event(loop=self.workerLoop)
		log(1, "creating ws server...")
		self.startQRunner = websockets.serve(self.tranceMitter, '127.0.0.1', 5678, loop=self.workerLoop)	
		log(1, "setting up tasks for the...")
		self.edelta.startAsyncio(self.workerLoop)
		for s in self.edelta.socket:
			self.workerLoop.create_task(self.nefariousTrawlerOfGutterSpanningEmissions(s))
		self.workerLoop.create_task(self.infernalEngineOfHiddenDesires())
		self.workerLoop.create_task(self.trancePlitter())		
		self.workerLoop.create_task(self.annaLiza(self.edelta))		
		self.workerLoop.create_task(self.renaLiza())		
		log(1, "about to create worker thread ...")
		self.workerThread = Thread(target=self.runEventLoop, args=(self.workerLoop, self.startQRunner))
		log(1, "starting worker thread and kicking off worker loop ...")
		self.workerThread.start()
		self.startTime = datetime.now()
		self.lastMarketRecheck = datetime.now()
		self.stopTime = None
		self.isRunning = True
		
	def stop(self):
		"""Stop the main event loop and worker thread. This should be restartable."""
		if not self.workerLoop:
			log(1, "not running")
			return
		log(1, "about to stop worker thread ...")
		asyncio.gather(*asyncio.Task.all_tasks(loop=self.workerLoop)).cancel() # Canceling pending tasks and stopping the loop
		self.workerLoop.stop() # Stopping the loop
		log(1, "about to cleanup edelta")
		self.edelta.close()
		log(1, "about to cleanup thread")
		self.workerThread.join()
		log(1, "about to cleanup event loop")
		self.workerLoop.close()
		self.workerLoop = None
		self.workerThread = None
		self.stopTime = datetime.now()
		self.isRunning = False
		
	def joStatus(self):
		if not self.isRunning:
			return  { "status": "stopped", "uptime": "", "ws": "ws://127.0.0.1:5678/" }
		status = "ok" 
		uptime = datetime.now() - self.startTime
		return { "status": status, "uptime": str(uptime), "ws": "ws://127.0.0.1:5678/" }
	
	def findTrade(self, side, token, price, buyer=None, seller=None, amount=0):
		"""
		looks for a matching trade with given characeristics
		- returns a list of matching trades
		"""
		found = []
#		log(1, "find trades")
#		log(1, "token " + token + ","+str(type(side)))
#		log(1, "side " + side + ","+str(type(side)))
#		log(1, "price " + str(price) + ","+str(type(price)))
#		log(1, "buyer " + str(buyer) + ","+str(type(buyer)))
#		log(1, "amount " + str(amount) + ","+str(type(amount)))
		if token in self.trades:
			trades = self.trades[token]
			for ik in trades:
				i = trades[ik]
				if (i.side == side) and (i.price == price) and (not buyer or (i.buyer == buyer)) and (not seller or (i.seller == seller)):
					found.append(i)
		else:
			log(1, "token "+token+" has no trades listing")
		return found
	
	def edMappingsJS(self, ed_token_adr_name='ed_token_adr', ed_token_decis_name='ed_token_decis', ed_adr_name_name='ed_adr_name',
					 header="/* automatically rebuilt on startup */\n"):
		"""
		converts the static info for the etherdelta config (ie config.js) to build javascript dict for mapping addresses onto token names and
		also mapping the decimal point for each token
		"""
		cm = header;
		ta = "var "+ed_token_adr_name+" = {\n"
		for i in self.edelta.tokenAddrs.keys():
			ta += "'" + i + "'" + ': ' +"'" + self.edelta.tokenAddrs[i]+ "'" + ',\n'
		ta += "}\n"
		td = "var "+ed_token_decis_name+" = {\n"
		for i in self.edelta.tokenDecimals.keys():
			td += "'" + i + "'" + ': ' + str(self.edelta.tokenDecimals[i]) + ',\n'
		td += "}\n"
		an = "var "+ed_adr_name_name+" = {\n"
		for i in self.edelta.addrName.keys():
			an += "'" + i + "'" + ': ' + "'" + self.edelta.addrName[i] + "'" + ',\n'
		an += "}\n"
		return cm + ta + td + an;
	
	def setExpressGasPrice(self, gp):
		self.xpressGasPrice_gwei = gp
		
	def setLazyGasPrice(self, gp):
		self.lazyGasPrice_gwei = gp
		
	def setMinimumViableSDGain_eth(self, mg):
		self.minimumViableSDGain_eth = mg
		
	def setMaximumViableSDGain_eth(self, mg):
		self.maximumViableSDGain_eth = mg
		
	def setParams(self, params):
		try:
			for k, sval in params.items():
				if k == "gasPrice":
					try:
						fvalue = float(sval)
						self.setExpressGasPrice(fvalue)
					except ValueError:
						return {"gasPrice": self.xpressGasPrice_gwei, "message": "gasPrice was not a float"}
				elif k == "minGain":
					try:
						fvalue = float(sval)
						self.setMinimumViableSDGain_eth(fvalue)
					except ValueError:
						return {"minGain": self.minGain_eth, "message": "minGain was not a float"}
				elif k == "maxGain":
					try:
						fvalue = float(sval)
						self.setMaximumViableSDGain_eth(fvalue)
					except ValueError:
						return {"maxGain": self.minGain_eth, "message": "minGain was not a float"}
		except Exception as e:
			return { "message": "setParams error "+str(e)}
		return {}
	
	def getParams(self):
		return { 'gasPrice': self.xpressGasPrice_gwei, 'minGain': self.minimumViableSDGain_eth, 'maxGain': self.maximumViableSDGain_eth}
		
	def __init__(self, wallet=None, mad_log_file=None, sweetness=None, sweet_filter=None, shit_token_filter=None,
				 xpressGasPrice_gwei=50, lazyGasPrice_gwei=4, minimumViableSDGain_eth=0.1, maximumViableSDGain_eth=0, marketCheckGap_secs=3600):
		global madLog, madLogging
		log(1, "creatin ether delta with no worker loop ...")
		self.w3 = w3w()

		self.xpressGasPrice_gwei = xpressGasPrice_gwei
		self.lazyGasPrice_gwei = lazyGasPrice_gwei
		self.minimumViableSDGain_eth = minimumViableSDGain_eth
		self.maximumViableSDGain_eth = maximumViableSDGain_eth
		self.marketCheckGap_secs = marketCheckGap_secs
		self.wallet = wallet
		
		self.edelta = EtherDelta()
		if sweetness is not None:
			log(1, "JoDolgozo() setting sugar magic to "+str(sweetness))
			self.sugarMagic = sweetness
		self.sweet_filter = None
		if sweet_filter is not None:
			log(1, "JoDolgozo() setting sweet filter to "+str(sweet_filter))
			self.sweet_filter = sweet_filter
		self.shit_token_filter = None
		if shit_token_filter is not None:
			log(1, "JoDolgozo() setting shit token filter to "+str(shit_token_filter))
			self.shit_token_filter = shit_token_filter
		madLogging = False
		madLog = None
		if mad_log_file is not None:
			madLogging = True
			madLog = open(mad_log_file, 'w')
		self.workerLoop = None
		self.workerThread = None
		self.startQRunner = None
		self.cueBall = set()
		self.msgQ = asyncio.Queue()
		self.startTime = None
		self.stopTime = None
		self.market = {} # a dict of MarketInfo objects, which holds a list of interesting Order objects
		self.trades = {} # a dict of the pure Trade lists processed from the JSON returned by getTrades
		self.deals = {} # the current orderId book for tokens of interest
		self.hasInitialTickerList = False
		self.reloadTickerList = False
		self.isRunning = False
		self.tokens = []
		self.tokensAdrIdx = {}
		# a list of TokenInfo objects, which holds a list of interesting TokenInfo objects.
		for i in self.edelta.tokenAddrs:
			adr = self.edelta.tokenAddrs[i]
			t = self.getTokenInfo(adr)
			t.name = i

from eth_utils import to_dict

from web3.providers.base import JSONBaseProvider  # noqa: E402
from web3.utils.compat import  make_post_request
from web3.utils.http import construct_user_agent

from requests.exceptions import ConnectionError
from http.client import RemoteDisconnected
from urllib3.exceptions import ProtocolError
from urllib3.exceptions import ReadTimeoutError
from requests.exceptions import ReadTimeout

def has_instance(e1, e2):
	if not isinstance(e1, Exception):
		return False
	if isinstance(e1, e2):
		return True
	try:
		a = next (e for e in e1.args if isinstance(e, e2) or has_instance(e, e2))
		return True
	except StopIteration:
		return False
	return True

# e = RemoteDisconnected('base')
# e1 = ReadTimeout('some shit')
# e2 = Exception('e2', e)
# e3 = Exception('e3', e1)
# e4 = Exception(e1)
# e5 = Exception('e5', e1)
# print(str(RemoteDisconnected))
# print(str(e) + " has ", has_instance(e, (RemoteDisconnected, ReadTimeoutError)))
# print(str(e1) + " has ", has_instance(e1, (RemoteDisconnected, ReadTimeoutError)))
# print(str(e2) + " has ", has_instance(e2, (RemoteDisconnected, ReadTimeout)))
# print(str(e3) + " has ", has_instance(e3, (RemoteDisconnected, ReadTimeoutError)))
# print(str(e4) + " has ", has_instance(e4, (RemoteDisconnected, ReadTimeout)))
# print(str(e5) + " has ", has_instance(e5, (RemoteDisconnected, ReadTimeout)))

class BentTPProvider(JSONBaseProvider):
	endpoint_uri = None
	_request_args = None
	_request_kwargs = None

	def __init__(self, endpoint_uri, request_kwargs=None):
		self.endpoint_uri = endpoint_uri
		self._request_kwargs = request_kwargs or {}
		super(BentTPProvider, self).__init__()

	def __str__(self):
		return "BentTP connection {0}".format(self.endpoint_uri)

	@to_dict
	def get_request_kwargs(self):
		if 'headers' not in self._request_kwargs:
			yield 'headers', self.get_request_headers()
		for key, value in self._request_kwargs.items():
			yield key, value

	def get_request_headers(self):
		return {
			'Content-Type': 'application/json',
			'User-Agent': construct_user_agent(str(type(self))),
		}

	def make_request(self, method, params):
		request_data = self.encode_rpc_request(method, params)
		kwargs = self.get_request_kwargs()
		immediate_retry_errs = (RemoteDisconnected, ReadTimeout, ReadTimeoutError)
		retries = 3
		while retries > 0:
			try:
				raw_response = make_post_request( self.endpoint_uri, request_data, **kwargs )
				break
			except immediate_retry_errs as e:
# 				print(str(e))
# 				print(str(e.args))
				retries -= 1
				if retries < 0:
					raise e
				print(str(e), "got a RemoteDisconnected and retrying, count = "+str(retries))
			except (ConnectionError, ProtocolError) as e:
# 				print(str(e))
# 				print(str(e.args))
				if has_instance(e, immediate_retry_errs):
					retries -= 1
					if retries < 0:
						raise e
						print(str(e), "got a ConnectionError wrapping RemoteDisconnected and retrying, count = "+str(retries))
				else:
					raise e
		response = self.decode_rpc_response(raw_response)
		return response

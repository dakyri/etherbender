'''
Created on Oct 9, 2017

@author: dak
'''
import signal
import sys
from worker import JoDolgozo
from time import sleep
worker = JoDolgozo()

def sigIntHandler(signal, frame):
	print("Ctr-C on command line. Cleaning up worker")
	worker.stop()
	sys.exit(0)
signal.signal(signal.SIGINT, sigIntHandler)

worker.start()

while True:
	pass
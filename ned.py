#async def echo(websocket, path):
#	print("in")
#	await websocket.send('getMarket')
#	print("sent")
#	async for message in websocket:
#		print(message)

#asyncio.get_event_loop().run_until_complete(
#    websockets.serve(echo, 'socket.etherdelta.com', 443))
#asyncio.get_event_loop().run_forever()
import asyncio
import cfscrape
from ether_delta import print_scrapage
import json
import sys
import requests
from wsio import wsio
from wsio import SioEvent
from aio_cf_session import aio_cf_session

try:
	from urlparse import urlparse
except ImportError:
	from urllib.parse import urlparse

async def hello(cfscraper, domain, config_url):
	print("in ... " + config_url )
	aioSession = aio_cf_session(cfscraper, domain)
	print("got session")
	async with aioSession.get(config_url) as resp:
		data = await resp.text()
		if data:
			print("got data ok")
		else:
			print("couldn't access config url proprely")
	sio_ws = wsio(aioSession, 'https://socket.etherdelta.com')
	try:
		await sio_ws.connect()
		print("got socket")
	except Exception as e:
		print("exception getting socket")
		print(str(e))
		aioSession.close()
		return
	for i in range(1,4):
		ev = await sio_ws.rx()
		print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>..")
		print(ev.event)
		print(ev.data)
		ev = await sio_ws.rx()
		print("......")
		print(ev.event)
		print(ev.data)

	await sio_ws.tx("getMarket", { "token": "0xb518d165398d9057ea8b73096edda5c7754bcd62"})
	print(">>>>>SEND COMMAND getMarket>>>>>>>>>..")
	ev = await sio_ws.rx()
	print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>..")
	print(ev.event)
	print(ev.data)
	ev = await sio_ws.rx()
	print("......")
	print(ev.event)
	print(ev.data)

	await sio_ws.close()
	aioSession.close()

cfscraper = None
jc = None
site_url_base='https://etherdelta.com'
config_path='config/main.json'
aioSession = None

try:
	config_url = site_url_base+'/'+config_path
	print("getting EtherDelta config from "+config_url+"...", file=sys.stderr)
	cfscraper = cfscrape.create_scraper()
		
	resp = cfscraper.get(config_url)
	print_scrapage(cfscraper)

	jc = json.loads(resp.text)
	print("EtherDelta object successfully set up...", file=sys.stderr)
	domain = urlparse(resp.url).netloc
except requests.exceptions.RequestException as e:
	print(e)
	sys.exit()

asyncio.get_event_loop().run_until_complete(hello(cfscraper, domain, config_url))
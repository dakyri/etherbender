'''
Created on Nov 5, 2017

light wrapper for character base socket/engine.io over websocket
@author: dak
'''

import json
import aiohttp
from aiohttp import WSMsgType
from aiohttp import WSCloseCode
from enum import Enum

class EIO:
# engine.io packet types
	OPEN = '0'		# Sent from the server when a new transport is opened (recheck)
	CLOSE = '1'		# Request the close of this transport but does not shutdown the connection itself
	PING = '2'		# Sent by the client. Server should answer with a pong packet containing the same data
	PONG = '3'		# Sent by the server to respond to ping packets.
	MESSAGE = '4'	# actual message, client and server should call their callbacks with the data.
	UPGRADE = '5'	# Before engine.io switches a transport, it tests, if server and client can communicate over this transpor
	NOOP	= '6'	# A noop packet. Used primarily to force a poll cycle when an incoming websocket connection is received.

class SIO:		
	# socket.io packet types
	CNX = '0'		# connect
	DISCNX = '1'	# disconnect
	EVENT = '2'		# i did a thing!
	ACK = '3'		# ack
	ERROR = '4'		# someone fucked up
	BIN_EV = '5'	# binary event
	BIN_ACK = '6'	# acknowledgement for binary event
	
class SioEvent:
	def __init__(self, event, data):
		self.event = event
		self.data = data

class wsio:
	
	async def rx(self):
		msg = await self.ws.receive()
		t = msg.type
		if t == WSMsgType.TEXT:
			sd = msg.data
			eiocode = sd[0]
			if eiocode == EIO.OPEN:
				jo = json.loads(sd[1:])
				if "sessionId" in jo:
					self.sessionId = jo["sessionId"]
				if "upgrades" in jo:
					self.upgrades = jo["upgrades"]
				if "pingInterval" in jo:
					self.pingInterval = int(jo["pingInterval"])
					print("ping interval at "+str(self.pingInterval))
				if "pingTimeout" in jo:
					self.pingTimeout = int(jo["pingTimeout"])
					print("ping timeout at "+str(self.pingInterval))
				return SioEvent('open', {})
			elif eiocode == EIO.CLOSE:
				return SioEvent('close', {})
			elif eiocode == EIO.PING:
				return SioEvent('ping', {})
			elif eiocode == EIO.PONG:
				return SioEvent('pong', {})
			elif eiocode == EIO.MESSAGE:
				siocode = sd[1]
				if siocode == SIO.CNX:
					return SioEvent('cnx', {})
				elif siocode == SIO.DISCNX:
					return SioEvent('discnx', {})
				elif siocode == SIO.EVENT:
					jo=json.loads(sd[2:])
					return SioEvent(jo[0], jo[1])
				elif siocode == SIO.ACK:
					return SioEvent('ack', {})
				elif siocode == SIO.ERROR:
					return SioEvent('error', {})
				elif siocode == SIO.BIN_EV:
					return SioEvent('bin_event', {})
				elif siocode == SIO.BIN_ACK:
					return SioEvent('bin_ack', {})
				return SioEvent('error', {'msg': 'unknown socket io type '+str(siocode)})
			elif eiocode == EIO.UPGRADE:
				return SioEvent('upgrade', {})
			elif eiocode == EIO.NOOP:
				return SioEvent('noop', {})
			return SioEvent('error', {'msg': 'unknown engine io type '+str(eiocode)})
		elif t == WSMsgType.BINARY:
			bd = msg.data
			return SioEvent('binary', {'data': bd })
		elif t == WSMsgType.CLOSE:
			code = msg.data
			if code == WSCloseCode.OK:
				return SioEvent('close', {'code': WSCloseCode.OK,  'msg':'OK'})
			else:
				return SioEvent('close', {'code': code, 'msg': msg.extra})
		elif t == WSMsgType.PING:
			return SioEvent('ping', {})
		elif t == WSMsgType.PONG:
			return SioEvent('pong', {})
		elif t == WSMsgType.ERROR:
			nm = (type(msg.data)).__name__
			return SioEvent('error', {'msg': 'web socket error '+nm, 'errorName':nm})
		elif t == WSMsgType.CLOSED:
			return SioEvent('closed', {})
		return SioEvent('error', {'msg': 'unknown web socket message type '+str(t)})
	
	async def tx(self, event, params={}):	
		msg = EIO.MESSAGE + SIO.EVENT + json.dumps([event, params])
		await self.ws.send_str(msg)
	
	async def connect(self):
		self.ws = await self.session.ws_connect(self.url + '/socket.io/?EIO=3&transport=websocket')
		return self.ws
	
	async def close(self):
		await self.ws.close()
		self.ws = None
		
	def __init__(self, session, url):
		self.sessionId = None
		self.upgrades = []
		self.pingInterval = 25000
		self.pingTimeout = 60000
		self.session = session
		self.url = url
		self.ws = None
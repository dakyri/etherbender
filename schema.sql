drop table if exists users;
drop table if exists ether_delta_stats;
drop table if exists ether_delta_deals;

create table users (
	id integer primary key autoincrement,
	google_id text not null,
	rights integer not null
);

create table ether_delta_stats (
	id 				integer primary key autoincrement,
	time_stamp		text not null,	
	start_time		text not null,
	n_buys			integer not null,
	n_sells			integer not null,
	ave_buy_price	real not null,
	min_buy_price	real not null,
	max_buy_price	real not null,
	ave_sell_price	real not null,
	min_sell_price	real not null,
	max_sell_price	real not null
);

create table ether_delta_deals (
	id				integer primary key autoincrement,
	order_id		integer not null,
	price			real not null,
	amount_get		integer not null,
	token_get		text not null,
	amount_give		integer not null,
	token_give		text not null,
	expires			text not null,
	nonce			text not null,
	order_user		text not null
);

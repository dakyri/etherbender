const BigNumber = require('bignumber.js');
const Web3 = require('web3');
const ABIEtherDelta = require('./contracts/etherdelta.json');
const ABIToken = require('./contracts/token.json');
const sha256 = require('js-sha256').sha256;
const ethUtil = require('ethereumjs-util');
const Tx = require('ethereumjs-tx');
// Instances of the Buffer class are similar to arrays of integers but correspond to fixed-sized,
// raw memory allocations outside the V8 heap.
// The size of the Buffer is established when it is created and cannot be resized.
function Trader() {
  const self = this;

  self.init = config => new Promise((resolve, reject) => {
    self.config = config;
    self.web3 = new Web3(new Web3.providers.HttpProvider(config.provider));
    self.contractEtherDelta = new self.web3.eth.Contract(ABIEtherDelta, config.addressEtherDelta);
    self.contractToken = new self.web3.eth.Contract(ABIToken);
    self.state = {
      orders: undefined,
      trades: undefined,
      myOrders: undefined,
      myTrades: undefined,
    };

  });
  
  /**
   * @param x javascript
   * @param d number of decimals in token
   */
  self.mko = (x, d) => {
	  return {
          id: x.id,
          date: new Date(x.updated),
          price: new BigNumber(x.price),
          amountGet: new BigNumber(x.amountGet),
          amountGive: new BigNumber(x.amountGive),
          deleted: x.deleted,
          expires: Number(x.expires),
          nonce: Number(x.nonce),
          tokenGet: x.tokenGet,
          tokenGive: x.tokenGive,
          user: x.user,
          r: x.r,
          s: x.s,
          v: x.v ? Number(x.v) : undefined,
          amount: self.toEth(x.amountGet, d).toNumber(),
//          availableVolume: Number(x.availableVolume),
//          ethAvailableVolume: Number(x.ethAvailableVolume),
//          availableVolumeBase: Number(x.availableVolumeBase),
//          ethAvailableVolumeBase: Number(x.ethAvailableVolumeBase),
	  }
  }

  self.getEtherDeltaBalance = (token, user) => new Promise((resolve, reject) => {
    self.contractEtherDelta.balanceOf(token === 'ETH' ? '0x0000000000000000000000000000000000000000' : token.addr, user.addr, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });

  self.getBalance = (token, user) => new Promise((resolve, reject) => {
    if (token === 'ETH') {
      self.web3.eth.getBalance(user.addr, (err, result) => {
        if (err) reject(err);
        resolve(result);
      });
    } else {
      self.contractToken.at(token.addr).balanceOf(user.addr, (err, result) => {
        if (err) reject(err);
        resolve(result);
      });
    }
  });

  self.getBlockNumber = () => new Promise((resolve, reject) => {
    self.web3.eth.getBlockNumber((err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });

  self.getNextNonce = user => new Promise((resolve, reject) => {
    self.web3.eth.getTransactionCount(user.addr, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });

  self.takeFraction = (user, order, fraction) => 
  		self.takeOrder(user, order, order.amountGet.times(new BigNumber(String(fraction))))

  self.takeOrder = (user, order, amt) => new Promise((resolve, reject) => {
    self.getNextNonce(user)
    .then((nonce) => {      
      const amount = new BigNumber(String(amt))
      self.contractEtherDelta.methods.testTrade(
        order.tokenGet,
        order.amountGet,
        order.tokenGive,
        order.amountGive,
        order.expires,
        order.nonce,
        order.user,
        order.v,
        order.r,
        order.s,
        amount,
        user.addr).call(
	        (errTest, resultTest) => {
	          if (errTest || !resultTest) {
	        	  reject('Order will fail');
	          }
	          const data = self.contractEtherDelta.methods.trade(
	            order.tokenGet,
	            order.amountGet,
	            order.tokenGive,
	            order.amountGive,
	            order.expires,
	            order.nonce,
	            order.user,
	            order.v,
	            order.r,
	            order.s,
	            amount).encodeABI();
	          console.log('abi')
	          console.log(data)
	          const options = {
	            gasPrice: self.config.gasPrice,
	            gasLimit: self.config.gasLimit,
	            nonce,
	            data,
	            to: self.config.addressEtherDelta,
	          };
	          console.log('tx')
	          const tx = new Tx(options);
	          console.log(tx)
	          tx.sign(new Buffer(user.key, 'hex'));
	          console.log('tx signed')
	          console.log(tx)
	          var txs = tx.serialize().toString('hex');
	          console.log(txs)
	          const rawTx = `0x${tx.serialize().toString('hex')}`;
	          self.web3.eth.sendSignedTransaction(rawTx, (err, result) => {
	            if (err) reject(err);
	            resolve(result);
	          });
	        }
        );
    })
    .catch((err) => {
      reject(err);
    });
  });

  self.toEth = (wei, decimals) => new BigNumber(String(wei))
    .div(new BigNumber(10 ** decimals));
  self.toWei = (eth, decimals) => new BigNumber(String(eth))
    .times(new BigNumber(10 ** decimals)).floor();
}

module.exports = Trader;

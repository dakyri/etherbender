
/*
 * setup stuff from the bot example
 */
const ABIEtherDelta = require('./contracts/etherdelta.json');
const ABIToken = require('./contracts/token.json');

self.contractED = self.web3.eth.contract(ABIEtherDelta).at(config.addressEtherDelta);

self.web3 = new Web3(new Web3.providers.HttpProvider(config.provider));
self.contractEtherDelta = self.web3.eth.contract(ABIEtherDelta).at(config.addressEtherDelta);
self.contractToken = self.web3.eth.contract(ABIToken);
/*
contractEtherDelta	"smart_contract/etherdelta.sol"
contractToken	"smart_contract/token.sol"
contractReserveToken	"smart_contract/reservetoken.sol"

contractEtherDeltaAddrs	
0	
addr	"0x8d12a197cb00d4747a1fe03395095ce2a5cc6819"
info	"Deployed 02/09/2017 -- THE CURRENT SMART CONTRACT"

 */

self.getNextNonce = user => new Promise((resolve, reject) => {
	self.web3.eth.getTransactionCount(user.addr, (err, result) => {
		if (err) reject(err);
		resolve(result);
	});
});


/*
 * take function from the bot example
 */
self.takeOrder = (user, order, fraction) => new Promise((resolve, reject) => {
	self.getNextNonce(user).then((nonce) => {
		const amount = order.amountGet.times(new BigNumber(String(fraction)));
		self.contractED.testTrade.call(
				order.tokenGet,
				order.amountGet,
				order.tokenGive,
				order.amountGive,
				order.expires,
				order.nonce,
				order.user,
				order.v,
				order.r,
				order.s,
				amount,
				user.addr,
				(errTest, resultTest) => {
					if (errTest || !resultTest) reject('Order will fail');
					const data = self.contractED.trade.getData(
							order.tokenGet,
							order.amountGet,
							order.tokenGive,
							order.amountGive,
							order.expires,
							order.nonce,
							order.user,
							order.v,
							order.r,
							order.s,
							amount);
					const options = {
							gasPrice: self.config.gasPrice,
							gasLimit: self.config.gasLimit,
							nonce,
							data,
							to: self.config.addressEtherDelta,
					};
					const tx = new Tx(options);
					tx.sign(new Buffer(user.pk, 'hex'));
					const rawTx = `0x${tx.serialize().toString('hex')}`;
					self.web3.eth.sendRawTransaction(rawTx, (err, result) => {
						if (err) reject(err);
						resolve(result);
					});
				});
	})
	.catch((err) => {
		reject(err);
	});
});
  
/*
 * the sign function from the bot example code
 */
const sign = (msgToSignIn, privateKeyIn) => {
	const prefixMessage = (msgIn) => {
		let msg = msgIn;
		msg = new Buffer(msg.slice(2), 'hex');
		msg = Buffer.concat([
			new Buffer(`\x19Ethereum Signed Message:\n${msg.length.toString()}`),
			msg]);
		msg = self.web3.sha3(`0x${msg.toString('hex')}`, { encoding: 'hex' });
		msg = new Buffer(msg.slice(2), 'hex');
		return `0x${msg.toString('hex')}`;
	};

	const privateKey = privateKeyIn.substring(0, 2) === '0x' ?
			privateKeyIn.substring(2, privateKeyIn.length) : privateKeyIn;
	const msgToSign = prefixMessage(msgToSignIn);
	try {
		const sig = ethUtil.ecsign(
				new Buffer(msgToSign.slice(2), 'hex'),
				new Buffer(privateKey, 'hex'));
		const r = `0x${sig.r.toString('hex')}`;
		const s = `0x${sig.s.toString('hex')}`;
		const v = sig.v;
		const result = { r, s, v, msg: msgToSign };
		return result;
	} catch (err) {
		throw new Error(err);
	}
};





// setup from sample bot code
self.web3 = new Web3(new Web3.providers.HttpProvider(config.provider));
self.contractEtherDelta = self.web3.eth.contract(ABIEtherDelta).at(config.addressEtherDelta);
self.contractToken = self.web3.eth.contract(ABIToken);

// calls from sample bot code
self.getBlockNumber = () => new Promise((resolve, reject) => {
	self.web3.eth.getBlockNumber((err, result) => {
		if (err) reject(err);
		resolve(result);
	});
});

self.getNextNonce = user => new Promise((resolve, reject) => {
	self.web3.eth.getTransactionCount(user.addr, (err, result) => {
		if (err) reject(err);
		resolve(result);
	});
});

self.takeOrder = (user, order, fraction) => new Promise((resolve, reject) => {
	self.getNextNonce(user)
	.then((nonce) => {
		const amount = order.amountGet.times(new BigNumber(String(fraction)));
		self.contractEtherDelta.testTrade.call(
				order.tokenGet,
				order.amountGet,
				order.tokenGive,
				order.amountGive,
				order.expires,
				order.nonce,
				order.user,
				order.v,
				order.r,
				order.s,
				amount,
				user.addr,
				(errTest, resultTest) => {
					if (errTest || !resultTest) reject('Order will fail');
					const data = self.contractEtherDelta.trade.getData(
							order.tokenGet,
							order.amountGet,
							order.tokenGive,
							order.amountGive,
							order.expires,
							order.nonce,
							order.user,
							order.v,
							order.r,
							order.s,
							amount);
					const options = {
							gasPrice: self.config.gasPrice,
							gasLimit: self.config.gasLimit,
							nonce,
							data,
							to: self.config.addressEtherDelta,
					};
					const tx = new Tx(options);
					tx.sign(new Buffer(user.pk, 'hex'));
					const rawTx = `0x${tx.serialize().toString('hex')}`;
					self.web3.eth.sendRawTransaction(rawTx, (err, result) => {
						if (err) reject(err);
						resolve(result);
					});
				});
	})
	.catch((err) => {
		reject(err);
	});
});

/*



Eth.sendRawTransaction(raw_transaction)
Delegates to eth_sendRawTransaction RPC Method
Sends a signed and serialized transaction.

>>> import rlp
>>> from ethereum.transactions import Transaction
>>> tx = Transaction(
    nonce=web3.eth.getTransactionCount(web3.eth.coinbase),
    gasprice=web3.eth.gasPrice,
    startgas=100000,
    to='0xd3cda913deb6f67967b99d67acdfa1712c293601',
    value=12345,
    data=b'',
)
>>> tx.sign(the_private_key_for_the_from_account)
>>> raw_tx = rlp.encode(tx)
>>> raw_tx_hex = web3.toHex(raw_tx)
>>> web3.eth.sendRawTransaction(raw_tx_hex)
'0xe670ec64341771606e55d6b4ca35a1a6b75ee3d5145a99d05921026d1527331'


start gas vs gas limit
----------------------
In that context, yes they are the same thing.

A start gas value, represents the maximum number of computational steps the transaction execution can take
(unused gas is refunded to the account sender).

Miner of the block receives a reward of (startgas - gas_rem) * gasprice.

If a transaction "runs out of gas" mid-execution, then all execution reverts, but the transaction is nevertheless valid,
and the only effect of the transaction is to transfer the entire sum startgas * gasprice to the miner.

If a user specifies a gas value that is too low, then it’s common for clients (e.g. Geth) to throw an error telling
the user that they’re not purchasing enough gas for this transaction to be executed. In contrast if the user sets
the gas too high then they may be effectively spending more gas that what was originally intended.

You can think of this as the total amount of gas a sender is willing to purchase for the transaction to be executed
i.e. gas limit or it's often referred to as gasUsed.

Additionally, gas limit is referenced when talking about blocks. They too, have a field called gas limit.
It defines the maximum amount of gas all transactions in the whole block combined are allowed to consume. Please see here.
 */

// setup web3 from website
self.loadWeb3 = function loadWeb3(callback) {
	if (typeof web3 !== "undefined" && typeof Web3 !== "undefined") {
		self.web3 = new Web3(web3.currentProvider);
		console.log("Connecting to MetaMask", web3.currentProvider);
		self.store.dispatch({
			type : "UPDATE_SETTINGS",
			value : { connection : { connection : "RPC", provider : self.config.ethProvider, testnet : self.config.ethTestnet } }
		});
		$("#pkDiv").hide();
		callback()
	} else if (typeof Web3 !== "undefined" && window.location.protocol !== "https:") {
		console.log("Connecting to Mist/Geth/Parity");
		self.web3 = new Web3(new Web3.providers.HttpProvider( self.config.ethProvider));
		try {
			var coinbase = self.web3.eth.coinbase;
			console.log("Coinbase: " + coinbase);
			self.store.dispatch({
				type : "UPDATE_SETTINGS",
				value : { connection : { connection : "RPC", provider : self.config.ethProvider, testnet : self.config.ethTestnet } }
			})
		} catch (err) {
			self.web3.setProvider(undefined);
			self.store.dispatch({
				type : "UPDATE_SETTINGS",
				value : { connection : { connection : "Proxy", provider : "https://etherscan.io", testnet : self.config.ethTestnet } }
			})
		}
		callback()
	} else {
		console.log("Connecting to Etherscan proxy");
		self.web3 = new Web3;
		self.store.dispatch({
			type : "UPDATE_SETTINGS",
			value : { connection : { connection : "Proxy", provider : "https://etherscan.io", testnet : self.config.ethTestnet } }
		});
		callback()
	}
};

// setup contracts from website
self.initContracts = function initContracts(callback) {
	self.utility.loadContract(self.web3, abiEtherDelta, self.selectedContract,
			function(err, contractEtherDelta) {
				self.contractEtherDelta = contractEtherDelta;
				self.utility.loadContract(self.web3, abiToken,
						"0x0000000000000000000000000000000000000000", function(errLoadContract, contractToken) {
							self.contractToken = contractToken;
							callback()
						})
			})
};

// setup network connection from website
self.initNetwork = function initNetwork(callback) {
	self.web3.version .getNetwork(function(error, version) {
		if (!error && version && Number(version) !== 1 && !self.config.ethTestnet) {
			self.dialogError("You are connected to the Ethereum testnet. Please connect to the Ethereum mainnet.")
		}
		var ethGasPriceCookie = self.utility.readCookie("ethGasPrice");
		if (ethGasPriceCookie) {
			var newEthGasPrice = JSON.parse(ethGasPriceCookie);
			if (newEthGasPrice > self.config.ethGasPrice) {
				self.setGasPrice(newEthGasPrice, true)
			} else {
				self.setGasPrice(self.config.ethGasPrice, true)
			}
		} else {
			self.setGasPrice(self.config.ethGasPrice, true)
		}
		callback()
	})
};

// sample call of send from website
self.utility.send(self.web3, self.ledgerEth, self.contractEtherDelta, self.selectedContract, "trade", [order.tokenGet, order.amountGet, order.tokenGive, order.amountGive, order.expires, order.nonce, order.user, v, r, s, amount, {
	gas: self.config.gasTrade,
	gasPrice: self.ethGasPrice,
	value: 0
}], self.accounts[self.selectedAccount].addr, self.accounts[self.selectedAccount].pk, self.accounts[self.selectedAccount].kind, self.nonce, function(errSend, resultSend) {
	self.nonce = resultSend.nonce;
	self.addPending(errSend, {
		txHash: resultSend.txHash
	});
	self.alertTxResult(errSend, resultSend);
	ga("send", {
		hitType: "event",
		eventCategory: "Action",
		eventAction: "Trade",
		eventLabel: self.selectedToken.name + "/" + self.selectedBase.name,
		eventValue: inputAmount
	})
})

// send from main.js website
utility.send = function send(web3, ledgerEth, contract, address, functionName,
		argsIn, fromAddress, privateKeyIn, addrKind, nonceIn, callback) {
	var privateKey = privateKeyIn && privateKeyIn.substring(0, 2) === "0x" ? privateKeyIn
			.substring(2, privateKeyIn.length)
			: privateKeyIn;

	function encodeConstructorParams(abi, params) {
		return abi.filter(
				function(json) {
					return json.type === "constructor" && json.inputs.length === params.length
				}).map(function(json) {
			return json.inputs.map(function(input) {
				return input.type
			})
		}).map(function(types) {
			return coder.encodeParams(types, params)
		})[0] || ""
	}
	var args = Array.prototype.slice.call(argsIn).filter(function(a) {
		return a !== undefined
	});
	var options = {};
	if (_typeof(args[args.length - 1]) === "object"
			&& args[args.length - 1].gas) {
		args[args.length - 1].gasPrice = args[args.length - 1].gasPrice
				|| config.ethGasPrice;
		args[args.length - 1].gasLimit = args[args.length - 1].gas;
		delete args[args.length - 1].gas
	}
	if (utils.isObject(args[args.length - 1])) {
		options = args.pop()
	}

	utility.getNextNonce(web3, fromAddress, function(err, nextNonce) {
		var nonce = nonceIn;
		if (nonceIn === undefined || nonceIn < nextNonce) {
			nonce = nextNonce
		}
		options.nonce = nonce;
		if (functionName === "constructor") {
			if (options.data.slice(0, 2) !== "0x") {
				options.data = "0x" + options.data
			}

			var encodedParams = encodeConstructorParams(contract.abi, args);
			console.log(encodedParams);
			options.data += encodedParams
		} else if (!contract || !functionName) {
			options.to = address
		} else {
			options.to = address;
			var functionAbi = contract.abi.find(function(element) {
				return element.name === functionName
			});
			var inputTypes = functionAbi.inputs.map(function(x) {
				return x.type
			});
			var typeName = inputTypes.join();
			options.data = "0x"
					+ sha3(functionName + "(" + typeName + ")").slice(0, 8)
					+ coder.encodeParams(inputTypes, args)
		}
		try {
			var post = function post(serializedTx) {
				var url = "https://"
						+ (config.ethTestnet ? config.ethTestnet : "api")
						+ ".etherscan.io/api";
				var formData = {
					module : "proxy",
					action : "eth_sendRawTransaction",
					hex : serializedTx
				};
				if (config.etherscanAPIKey)
					formData.apikey = config.etherscanAPIKey;
				utility.postURL(url, formData, function(errPostURL, body) {
					if (!errPostURL) {
						try {
							var result = JSON.parse(body);
							if (result.result) {
								callback(undefined, { txHash : result.result, nonce : nonce + 1 })
							} else if (result.error) {
								callback(result.error.message, { txHash : undefined, nonce : nonce })
							}
						} catch (errTry) {
							callback(errTry, { txHash : undefined, nonce : nonce })
						}
					} else {
						callback(errPostURL, { txHash : undefined, nonce : nonce })
					}
				})
			};
			if (web3.currentProvider && addrKind === "MetaMask") {
				options.from = fromAddress;
				options.gas = options.gasLimit;
				delete options.gasLimit;
				web3.eth.sendTransaction(options, function(errSend, hash) {
					if (!errSend) {
						callback(undefined, { txHash : hash, nonce : nonce + 1 })
					} else {
						callback(errSend, { txHash : undefined, nonce : nonce })
					}
				})
			} else if (ledgerEth && addrKind === "Ledger") {
				options.chainId = config.ethChainId;
				var tx = new Tx(options);
				tx.v = new Buffer([ config.ethChainId ]);
				var rawTx = tx.serialize().toString("hex");
				ledgerEth.signTransaction_async(config.ledgerPath, rawTx).then(
						function(result) {
							var optionsPlugSig = Object.assign(options, {
								v : new Buffer(result.v, "hex"),
								r : new Buffer(result.r, "hex"),
								s : new Buffer(result.s, "hex"),
								chainId : config.ethChainId
							});
							var txSigned = new Tx(optionsPlugSig);
							var serializedTx = txSigned.serialize().toString( "hex");
							post(serializedTx)
						}).fail(function(errFail) {
					console.log(errFail);
					callback("Failed to sign transaction", { txHash : undefined, nonce : nonce })
				})
			} else if (privateKeyIn) {
				var _tx = new Tx(options);
				utility.signTx(web3, fromAddress, _tx, privateKey,
						function(errSignTx, txSigned) {
							if (!errSignTx) {
								var serializedTx = txSigned.serialize() .toString("hex");
								post(serializedTx)
							} else {
								console.log(err);
								callback("Failed to sign transaction", { txHash : undefined, nonce : nonce })
							}
						})
			} else {
				callback("Failed to sign transaction", { txHash : undefined, nonce : nonce })
			}
		} catch (errCatch) {
			callback(errCatch, { txHash : undefined, nonce : nonce })
		}
	})
};

utility.txReceipt = function txReceipt(web3, txHash, callback) {
	function proxy() {
		var url = "https://"
				+ (config.ethTestnet ? config.ethTestnet : "api")
				+ ".etherscan.io/api?module=proxy&action=eth_GetTransactionReceipt&txhash="
				+ txHash;
		if (config.etherscanAPIKey)
			url += "&apikey=" + config.etherscanAPIKey;
		utility.getURL(url, function(err, body) {
			if (!err) {
				var result = JSON.parse(body);
				callback(undefined, result.result)
			} else {
				callback(err, undefined)
			}
		})
	}
	try {
		if (web3.currentProvider) {
			try {
				web3.eth.getTransactionReceipt(txHash, function(err, result) {
					if (err) {
						proxy()
					} else {
						callback(undefined, result)
					}
				})
			} catch (err) {
				proxy()
			}
		} else {
			proxy()
		}
	} catch (err) {
		proxy()
	}
};

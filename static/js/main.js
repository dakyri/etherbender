/**
 * 
 */

function displayDeci(amt, tok)
{
	var nm = ed_adr_name[tok];
	var d;
	if (! ed_token_decis.hasOwnProperty(nm)) {
		d = 18;
	} else {
		d = ed_token_decis[nm]
	}	
	var omt = amt
	while (d > 0) {
		amt = amt / 10.0;
		d--;
	}
//	window.alert('amt = '+omt + '-> '+amt)
	return amt;
}

function printName(tok) {
	if (! ed_adr_name.hasOwnProperty(tok)) {
		return tok;
	}
	return ed_adr_name[tok];
}

function orderInfoStr(ord) {
	var s = '';
	s += '<div>'
	s += 'Token '+ printName(ord.tokenGet) + ', price ' + ord.price;
	s += '</div>\n'
	if (ord.user) {
		s += '<div>'
		s += 'user ' + ord.user;
		s += '</div>\n'
	}
	s += '<div>'
	s += 'Volume ' + displayDeci(ord.availableVolume, ord.tokenGet) + ' / ' + displayDeci(ord.amountGet, ord.tokenGet)
	s += '</div>\n'
	if (ord.timestamp || ord.qTimestamp) {
		s += '<div>'
		if (ord.timestamp) {
			s += 'ED update time '+ ord.timestamp.format("YY/M/D k:mm:ss") + '</br>';
		}
		if (ord.qTimestamp) {
			s += 'EB q time '+ ord.qTimestamp.format("YY/M/D k:mm:ss") + '</br>';
		}
		s += '</div>\n'
	}
	
	if (ord.status || ord.description) {
		s += '<div>'
		if (ord.status && ord.status != 'OK') {
			s += '<span class="deal-status">'+ ord.status + '</span>';
		}
		if (ord.description) {
			s += '<div class="deal-description">' + ord.description + '</div>';
		}
		s += '</div>\n'
	}
	
	return s;
}

function tradeInfoStr(trd) {
	var nm = printName(trd.tokenGet);
	var s = '';
	s += '<div>'
	s += 'Trade '+ trd.txHash + ', amt ' + displayDeci(trd.amount, trd.tokenGet) + ', price ' + trd.price + ', buyer ' + trd.buyer
			+ ', trade time  '+ trd.timestamp.format("YY/M/D k:mm:ss");
	s += '</div>'
	if (trd.status || td.description) {
		s += '<div>'
		if (trd.status) {
			s += '<span class="deal-status">'+ trd.status + '</span>';
		}
		if (trd.description) {
			s += '<div class="deal-description">' + trd.description + '</div>';
		}
		s += '</div>\n'
	}
	
	return s;
}

function tokDivStr(t)
{
	/*
	var headStr = '('+obj.startTime + ' - ' + obj.time + ')'
	var buysStr = 'Buys: ' + 'total '+ obj.nBuys + ', average price ' + obj.aveBuyPrice
		+ ', min ' + obj.minBuyPrice + ', max ' + obj.maxBuyPrice;
	var sellStr = 'Sells: ' + 'total '+ obj.nSells + ', average price ' + obj.aveSellPrice 
		+ ', min ' + obj.minSellPrice + ', max ' + obj.maxSellPrice;
*/
	var d = '<div id="tokDiv%1" class="token-index-item">\n'
		+'<input type="checkbox" id="check%1" name="search" value="%1">\n'
		+'<label for="check%1">%1</label>'
		+' '+'<span class="token-addr">(%2)</span>'
		+'</div>\n'
	return d.replace(/%1/g, t.name).replace(/%2/g, t.addr)
}



function showTokenList(obj)
{
	var h = ''
	for (var k=0; k<obj.length; k++) {
		h += tokDivStr(obj[k])
	}
	$('#tokenIdx').html(h)
}

/*
 * principally this involves
 */
function fixObjectTimestamps(o)
{
	if (o.hasOwnProperty("timestamp") && o.timestamp != null) {
		o.timestamp = moment(o.timestamp)
	}
	if (o.hasOwnProperty("qTimestamp") && o.qTimestamp != null) {
		o.qTimestamp = moment(o.qTimestamp)
	}
	if (o.hasOwnProperty("startTime") && o.startTime != null) {
		o.startTime = moment(o.startTime)
	}

}


function showOrderBook(obj)
{
	fixObjectTimestamps(obj)
	var d = $('#order_book_'+obj.token);
	if (!d.length) {
		d = $('<div id="' +'order_book_'+obj.token + '" class="token-order-info"></div>')
		$("#orderBook").append(d)
	}
	var htmlStr = '';
	var len = 0
	for (oid in obj.sweets) {
		var sweet = obj.sweets[oid]
		fixObjectTimestamps(sweet)
		htmlStr += '<div class="token-order">'+orderInfoStr(sweet)+'</div>'
		len++
	}
	if (len == 0) {
		$(d).hide()
		$(d).html('')
	} else {
		$(d).show()
		$(d).html(htmlStr)
	}
}

function showOrder(obj)
{
	var d = $('#order_'+obj.oid);
	if (!d.length) {
		d = $('<div id="' +'order_'+obj.oid + '" class="moving-order-info"></div>')
		$('#orderDetails').append(d);
	}
	var dd = $('#order_detail_'+obj.oid);
	if (!dd.length) {
		dd = $('<div id="' +'order_detail_'+obj.oid + '" class="moving-order-detail"></div>')
		$(d).append(dd);
	}	
	$(dd).html(orderInfoStr(obj))
}


function showTrade(obj)
{
	oid = obj.orderId
	var d = $('#order_'+oid);
	if (!d.length) {
		d = $('<div id="' +'order_'+oid + '" class="moving-order-info"></div>')
		$('#orderDetails').append(d);
	}
	var td = $('#order_trade_'+obj.txHash);
	if (!td.length) {
		td = $('<div id="' +'order_trade_'+obj.txHash + '" class="moving-order-trade"></div>')
		$(d).append(td);
	}	

	$(td).html(tradeInfoStr(obj))
}

var DFLT_RETRY_COUNT = 3;
var DFLT_TIMEOUT_SECS = 15;

function BenderServer(_serverURL)
{
	var ajaxRetryCount = DFLT_RETRY_COUNT;
	var ajaxTimeoutMillis = DFLT_TIMEOUT_SECS*1000;
	var serverURL = _serverURL;
	
	this.postWorkerStart = function(onSuccess) {
		var requestURL = serverURL+'/worker/start';
		$.ajax({
			type: "POST",
			url: requestURL,
			dataType: "json",
			timeout: ajaxTimeoutMillis,
		    tryCount: 0,
		    retryLimit: ajaxRetryCount,
			error: function (xhr, textStatus, errorName) {
				showError(testStatus, "error", 10000, errorName, 'server start request');
			},
		    complete: function(request, status) {
		    },
			success: onSuccess
		});
	}

	this.postWorkerShutdown = function(onSuccess) {
		var requestURL = serverURL+'/worker/shutdown';
		$.ajax({
			type: "POST",
			url: requestURL,
			dataType: "json",
			timeout: ajaxTimeoutMillis,
		    tryCount: 0,
		    retryLimit: ajaxRetryCount,
			error: function (xhr, textStatus, errorName) {
				showError(testStatus, "error", 10000, errorName, 'server start request');
			},
		    complete: function(request, status) {
		    },
			success: onSuccess
		});
	}

	this.postWorkerParams = function(params, onSuccess) {
		var requestURL = serverURL+"/worker/params";
		$.ajax({
			type: "POST",
			url: requestURL,
			dataType: "json",
			data: params,
			timeout: ajaxTimeoutMillis,
		    tryCount: 0,
		    retryLimit: ajaxRetryCount,
			error: function (xhr, textStatus, errorName) {
				showError(testStatus, "error", 10000, errorName, 'server start request');
			},
		    complete: function(request, status) {
		    },
			success: function(data, status, request) {
				message = null
				for (var field in data) {
					v = data[field]
					switch (field) {
						case 'message': {
							message = v
							break;
						}
						case 'gasPrice': {
							$('#showGasPrice').text(v)
							break;
						}
						case 'minGain': {
							$('#showMinGain').text(v)
							break;
						}
						case 'maxGain': {
							$('#showMaxGain').text(v)
							break;
						}
					}
				}
				if (message != null) {
					showError(testStatus, "error", 10000, errorName, message);
				} else if (onSuccess != null) {
					onSuccess(data, status, request);
				}
			}
		});
	}

	this.postWorkerPing = function(onSuccess) {
		var requestURL = serverURL+"/worker/ping";
		$.ajax({
			type: "POST",
			url: requestURL,
			dataType: "json",
			timeout: ajaxTimeoutMillis,
		    tryCount: 0,
		    retryLimit: ajaxRetryCount,
			error: function (xhr, textStatus, errorName) {
				showError(testStatus, "error", 10000, errorName, 'server start request');
			},
		    complete: function(request, status) {
		    },
			success: onSuccess
		});
	}

}

var server = new BenderServer('http://'+self.location.host)
var webSocket = null;
var tokenList = null;
var tokenListAdrIdx = null;

window.server = server;
/*
 * set token list, as an array to preserve received order, and use a separate index which is built up here
 * maybe check through previous versions for any interesting tidbits not in the normal ticker stuff
 */
function setTokenList(nsl) {
	var osl = tokenList
	var oslidx = tokenListAdrIdx
	
	tokenList = nsl
	tokenListAdrIdx = {}
	for (var i=0; i<tokenList.length; i++) {
		var t = tokenList[i]
		tokenListAdrIdx[t.addr] = t
	}
}

function processJsonResponse(message)
{
	for (var subMsgType in message) {
		var subMsg = message[subMsgType]
		switch (subMsgType) {
		case 'tokens': {
			setTokenList(subMsg)
			showTokenList(subMsg)
			break;
		}
		case 'xinfo': {
			showOrderBook(subMsg)
			break;
		}
		case 'order': {
			fixObjectTimestamps(subMsg)
			showOrder(subMsg)
			break;
		}
		case 'trade': {
			fixObjectTimestamps(subMsg)
			showTrade(subMsg)
			break;
		}
		case 'server': {
			showError(subMsg.status+", "+subMsg.uptime, "status", 10000, '', 'Worker')
			break;
		}
		case 'err': {
			if (typeof subMsg == 'string') {
				showError(subMsg, "error", 10000, '', 'Worker')
			} else if (subMsg.hasOwnProperty('msg')) {
				showError(subMsg.msg, "error", 10000, '', 'Worker') 
			} else {
				showError('badly formatted error message on socket', "error", 10000, '', 'Worker')
			}
			break;
		}
		default: {
			showError('unknown message type "'+subMsgType+'"', "error", 10000, '', 'Worker')
			break;
		}
		}
	}
}

/**
 * stop and clear the current web socket in the globa variable 'websocket'
 * @returns
 */
function stopWebsocketClient() {
	if (webSocket != null) {
		webSocket.close();
		webSocket = null;
	}
}

/**
 *  start a websocket. check that we don't have an open connection  first, and then go for it
 * @param wsUri
 * @returns
 */
function startWebsocketClient(wsUri) {
	if (webSocket != null) {
		return webSocket;
	}
	try {
		webSocket = new WebSocket(wsUri);
	} catch (e) {
		showError(e.message, "error", 10000, e.name, 'Websocket open');
		return null;
	}

	webSocket.onopen = function (event) {
    };

    webSocket.onmessage = function (event) {
    	try {
            var message = JSON.parse(event.data); 
            processJsonResponse(message)
//            console.log(event.data)
    	} catch (e) {
    		showError(e.message + '(row '+e.lineNumber+', col '+e.columnNumber+')', "error", 10000, e.name, 'Websocket message parse error');
//    		  console.log(e.fileName);
//    		  console.log(e.stack)	
    	}
    };

    webSocket.onclose = function (event) {
        var reason;
//        alert(event.code);
        // See http://tools.ietf.org/html/rfc6455#section-7.4.1
        if (event.code == 1000)
            reason = "Normal closure, meaning that the purpose for which the connection was established has been fulfilled.";
        else if(event.code == 1001)
            reason = "An endpoint is \"going away\", such as a server going down or a browser having navigated away from a page.";
        else if(event.code == 1002)
            reason = "An endpoint is terminating the connection due to a protocol error";
        else if(event.code == 1003)
            reason = "An endpoint is terminating the connection because it has received a type of data it cannot accept (e.g., an endpoint that understands only text data MAY send this if it receives a binary message).";
        else if(event.code == 1004)
            reason = "Reserved. The specific meaning might be defined in the future.";
        else if(event.code == 1005)
            reason = "No status code was actually present.";
        else if(event.code == 1006)
           reason = "The connection was closed abnormally, e.g., without sending or receiving a Close control frame";
        else if(event.code == 1007)
            reason = "An endpoint is terminating the connection because it has received data within a message that was not consistent with the type of the message (e.g., non-UTF-8 [http://tools.ietf.org/html/rfc3629] data within a text message).";
        else if(event.code == 1008)
            reason = "An endpoint is terminating the connection because it has received a message that \"violates its policy\". This reason is given either if there is no other sutible reason, or if there is a need to hide specific details about the policy.";
        else if(event.code == 1009)
           reason = "An endpoint is terminating the connection because it has received a message that is too big for it to process.";
        else if(event.code == 1010) // Note that this status code is not used by the server, because it can fail the WebSocket handshake instead.
            reason = "An endpoint (client) is terminating the connection because it has expected the server to negotiate one or more extension, but the server didn't return them in the response message of the WebSocket handshake. <br /> Specifically, the extensions that are needed are: " + event.reason;
        else if(event.code == 1011)
            reason = "A server is terminating the connection because it encountered an unexpected condition that prevented it from fulfilling the request.";
        else if(event.code == 1015)
            reason = "The connection was closed due to a failure to perform a TLS handshake (e.g., the server certificate can't be verified).";
        else
            reason = "Unknown reason";
        
        console.log('Websocket close: '+reason);
    };
    
    webSocket.onerror = function (event) {
        showError('Communications error', "error", 10000, 'Unspecified error', 'Websocket')
    };

	return webSocket;
}

/****************************************************
 *  INTERFACE CALLBACKS
 ****************************************************/
/**
 *  respond to the start item in the server menu
 * @returns
 */
function onServerStartClick() {
	server.postWorkerStart(function(data, status, request) {
		if (data.hasOwnProperty('server')) {
			var sd = data['server']
			if (sd.hasOwnProperty('ws')) {
				startWebsocketClient(sd['ws'])
			}
		}
	})
}

/**
 *  respond to the stop item in the server menu
 * @returns
 */
function onServerStopClick() {
	stopWebsocketClient()
}

/**
 *  respond to the shutdown item in the server menu
 * @returns
 */
function onServerShutdownClick() {
	server.postWorkerShutdown(function(data, status, request) {
		processJsonResponse(data)
		stopWebClientSocket();
	})
}

function wsState(ws) {
	if (ws == null) {
		return "NON-EXISTENT";
	}
	switch (ws.readyState) {
	case 0: return "CONNECTING"
	case 1: return "OPEN"
	case 2: return "CLOSING"
	case 3: return "CLOSED"
	}
}

/**
 *  respond to the ping item in the server menu
 * @returns
 */
function onServerPingClick() {
	showError('Web socket '+wsState(webSocket), "info", 10000, 'Information', 'Websocket')
	server.postWorkerPing(function(data, status, request) {
		processJsonResponse(data)
	})

}

/**
 *  respond to the show sweet deals item in the server menu
 * @returns
 */
function onViewEDSweetsClick() {
	
}

function quickEdit(shower, inputer, serverParamId) {
	$('#'+shower).hide();
	$('#'+inputer).val($('#'+shower).text())
	$('#'+inputer).keydown(function(event) {
		if (event.keyCode == 13) {
			v =  $('#'+inputer).val();
			$('#'+shower).text(v);
			params = {}
			params[serverParamId] = v
			server.postWorkerParams(params, function(){})
			$('#'+shower).show();
			$('#'+inputer).hide();
			$('#'+inputer).off('keydown');
		}
	});
	$('#'+inputer).show();
}

$(document).ready(function(){
	$('#showGasPrice').click(function() { quickEdit('showGasPrice', 'setGasPrice', 'gasPrice'); });
	$('#showMinGain').click(function() { quickEdit('showMinGain', 'setMinGain', 'minimumGain'); });
	$('#showMaxGain').click(function() { quickEdit('showMaxGain', 'setMaxGain', 'maximumGain'); });
});

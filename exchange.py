'''
Created on Jan 14, 2018

@author: dak
'''

import iso8601
from rydrs import lintr
from datetime import datetime
from datetime import timedelta

# a trade object, as returned from the trade related api calls from ED
class Trade:
	"""
	 a trade object, as returned from the trade related api calls from ED
	"""
	def __str__(self):
		return "Trade " + str(self.side) + ' ' + str(self.tokenAddr)+ ", price " + str(self.price) + ", amount " + str(self.amount)\
			+ ", buyer " + str(self.buyer) + ", seller " + str(self.seller)
	
	def __init__(self, txHash=None, tokenAddr=None, side="buy", price=0, amt=0, base=0, buy=None, sell=None, timestamp=None):
		self.txHash = txHash
		self.side = side
		self.tokenAddr = tokenAddr
		self.price = price
		self.amount = amt
		self.amountBase = base
		self.timestamp = timestamp
		if timestamp:
			if isinstance(timestamp, str):
				self.timestamp = iso8601.parse_date(timestamp)
			elif isinstance(timestamp, datetime):
				self.timestamp = timestamp
		self.buyer = buy
		self.seller = sell
		self.orderId = ''
		self.status = "OK"
		self.description = ""
		self.src = ""
	
# an orderId object, as returned from the orderId related api calls from ED
class Order:
	"""
	an orderId object, as returned 
	from the orderId related api calls from ED
	"""
	
	@staticmethod
	def fromd(d, **kwargs):
		"""
		constructs an order from the given dictionary, adding in the kwargs
		"""
		if 'id' in d:
			d['oid'] = d['id']
			del d['id']
		if 'amount' in d:
			d['side'] = ('sell' if lintr(d['amount']) < 0 else 'buy')
			del d['amount']
		if 'price' in d:
			d['price'] = float(d['price'])
		if 'amountGet' in d:
			d['aget'] = lintr(d['amountGet'])
			del d['amountGet']
		if 'tokenGet' in d:
			d['tget'] = d['tokenGet']
			del d['tokenGet']
		if 'amountGive' in d:
			d['agiv'] = lintr(d['amountGive'])
			del d['amountGive']
		if 'tokenGive' in d:
			d['tgiv'] = d['tokenGive']
			del d['tokenGive']
		if 'availableVolume' in d:
			d['avol'] = lintr(d['availableVolume'])
			del d['availableVolume']
		if 'nonce' in d:
			d['nons'] = int(d['nonce'])
			del d['nonce']
		if 'updated' in d:
			d['timestamp'] = d['updated']
			del d['updated']
		if 'expires' in d:
			d['exp'] = int(d['expires'])
			del d['expires']
		if 'amountFilled' in d:
			del d['amountFilled']
		if 'ethAvailableVolumeBase' in d:
			del d['ethAvailableVolumeBase']
		if 'availableVolumeBase' in d:
			del d['availableVolumeBase']
		if 'ethAvailableVolume' in d:
			del d['ethAvailableVolume']
		d.update(**kwargs)
		return Order(**d)
	
	def token(self):
		return self.tokenGet if self.side == 'buy' else self.tokenGive
	
	def __str__(self):
		return ("Order "+ str(self.oid) + ", tokenGet " + str(self.tokenGet) + ", " + str(self.side) + ", price " + str(self.price)
			 + " user " + str(self.user) + ", expires " + str(self.expires)  + ", vol " + str(self.availableVolume) + " / "+ str(self.amountGet))

	def __init__(self, oid=None, side="buy", price=0, user=None, aget=0, tget=None, agiv=0, tgiv=None, avol=0, exp=0, nons=None, timestamp=None,
				 v=None, r=None, s=None):
		self.oid = str(oid)
		self.side = side
		self.price = price
		self.amountGet = aget
		self.tokenGet = tget
		self.amountGive = agiv
		self.tokenGive = tgiv
		self.availableVolume = avol
		self.expires = exp
		self.nonce = nons
		self.user = user
		self.v = v
		self.r = r
		self.s = s
		self.timestamp = None
		if timestamp:
			if isinstance(timestamp, str):
				self.timestamp = iso8601.parse_date(timestamp)
			elif isinstance(timestamp, datetime):
				self.timestamp = timestamp
		if not self.timestamp:
			self.timestamp = datetime.now()
		self.qTimestamp = None # internal timestamp for when we first get queued
		self.tTimestamp = None # internal timestamp each time we check it for being traded
		self.trades = []
		self.status = "OK"
		self.description = ""
		self.src = ""


class Exchange:
	def __init__(self):
		pass

"""
Created on Sep 10, 2017

@author: dak
"""
import os
import sys
import click
from flask import Flask, g, session, jsonify, render_template, request, flash, redirect
from sqlite3 import dbapi2 as sqlite3
from flask_oauth2_login import GoogleLogin
from flask_bootstrap import Bootstrap
from worker import JoDolgozo
import signal
from flask_bootstrap import WebCDN
from web3_trade import User

#a = ExchangeInfo()
#a2 = Order("1")
#a3 = Order("2")
#a.add(a2)
#a.add(a3)
#b=jsonDumpable(a)
#print(str(a))
#print(json.dumps(b))

def parse_funk(src, name):
	funk = None
	if src is not None:
		try:
			print(name + ' is: '+str(src))
			b = eval(compile(src, '<string>', 'eval'))
			if callable(b):
				funk = b
		except SyntaxError as e:
			print('syntax error parsing '+name+': '+str(e))
		except Exception as e:
			print('wierd exception parsing '+name+': '+str(e))
	return funk

flaskCommand = None
if len(sys.argv) >= 2:
	flaskCommand = sys.argv[1]
	
app = Flask(__name__)

print("about to process config ... ")

# set up default config and then override this from an environment variable if exists
app.config.update(dict(
	DATABASE=os.path.join(app.root_path, 'ebender.db'),
	DEBUG=True,
	SECRET_KEY='CorrectHorseBatteryStaple',
	GOOGLE_LOGIN_REDIRECT_SCHEME="http",
	USERNAME='some_username',
	PASSWORD='some_password',
	GOOGLE_LOGIN_CLIENT_ID='877688205998-agkco59b3esailbp533sddhoh78rg6j5.apps.googleusercontent.com',
	GOOGLE_LOGIN_CLIENT_SECRET='-xMnBunvWnQaPH6iYJ62Mahr'
))
app.config.from_json('booze', silent=True)

google_login = GoogleLogin(app)
mock_login = True
xtrm_debug = None
if 'XDEBUG' in os.environ:
	xtrm_debug = os.environ['XDEBUG']
worker = None
sweetness = 1
mad_logging = None
sweet_filter_src = None
sweet_filter = None
shit_token_filter_src = None
shit_token_filter = None
wallet = None
minimumViableSDGain_eth = 0.1
maximumViableSDGain_eth = 0
lazyGasPrice_gwei = 4
xpressGasPrice_gwei = 60

if 'SWEETNESS' in app.config:
	sweetness = app.config['SWEETNESS']
if 'XPRESS_GAS_PRICE' in app.config:
	xpressGasPrice_gwei = app.config['XPRESS_GAS_PRICE']
if 'LAZY_GAS_PRICE' in app.config:
	lazyGasPrice_gwei = app.config['LAZY_GAS_PRICE']
if 'MIN_VIABLE_SD_GAIN' in app.config:
	minimumViableSDGain_eth = app.config['MIN_VIABLE_SD_GAIN']
if 'MAX_VIABLE_SD_GAIN' in app.config:
	maximumViableSDGain_eth = app.config['MAX_VIABLE_SD_GAIN']
if 'SWEET_FILTER' in app.config:
	sweet_filter_src = app.config['SWEET_FILTER']
if 'SHIT_TOKEN_FILTER' in app.config:
	shit_token_filter_src = app.config['SHIT_TOKEN_FILTER']
if 'MAD_LOGGING' in app.config:
	mad_logging = app.config['MAD_LOGGING']
if 'WALLET_ADR' in app.config and 'WALLET_PK' in app.config:
	wallet = User(app.config['WALLET_ADR'], app.config['WALLET_PK'])
	
print('sweetness is '+str(sweetness))
print('xpressGasPrice_gwei is '+str(xpressGasPrice_gwei))
print('lazyGasPrice_gwei is '+str(lazyGasPrice_gwei))
print('minimumViableSDGain_eth is '+str(minimumViableSDGain_eth))
print('maximumViableSDGain_eth is '+str(maximumViableSDGain_eth))

if sweet_filter_src:
	sweet_filter = parse_funk(sweet_filter_src, "sweet filter")
if sweet_filter is None:
	print('no sweet filter')
	
if shit_token_filter_src:
	shit_token_filter = parse_funk(shit_token_filter_src, "shit filter")
if shit_token_filter is None:
	print('no shit token filter')
	
if wallet is None:
	print("*** dry run. no wallet ***")
else:
	print("*** running with an actual wallet ***")
	
def sigIntHandler(signal, frame):
	"""Command line interrupt handler"""
	global worker
	print("Ctr-C on command line. Cleaning up worker")
	if worker:
		worker.stop()
	sys.exit(0)
	
signal.signal(signal.SIGINT, sigIntHandler)

if flaskCommand == "run":
	Bootstrap(app)
	app.extensions['bootstrap']['cdns']['jquery'] = WebCDN('//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/')
	worker = JoDolgozo(mad_log_file=mad_logging, sweetness=sweetness, sweet_filter=sweet_filter, shit_token_filter=shit_token_filter, wallet=wallet)
	with open('static/js/ed_mappings.js', 'w') as f:
		f.write(worker.edMappingsJS())
	print(worker)
#	worker.start()

print("flask definitions ...")
@google_login.login_success
def login_success(token, profile):
	"""Actions taken on a successul login. Will try to verify the login to a local user database with specific rights"""
	gid = profile.get('email', None)

	if (not gid):
		flash("Expected google id in login response", "error")
		return render_template('main.html');  
	print("Trying to login as "+gid, file=sys.stderr)
	if (not set_login(gid)):
		return render_template('main.html');
	print("Successfully logged in as "+gid, file=sys.stderr)
	return render_view_edo_command(gid)

@google_login.login_failure
def login_failure(e):
	"""Actions taken on a failed login """
	set_logout()
	flash(str(e), "error")
	return render_template('main.html');  

def set_logout():
	"""Clear record of user id from our session """
	session.pop('logged_in', None)
	session.pop('logged_in_id', None);
	session.pop('logged_in_gid', None);

def set_login(googleid):
	"""Add details and rights of a user to our session """
	db = get_db()
	
	cur = db.execute("select id, rights from users where google_id=?", [googleid])
	row = cur.fetchone()
	if (row):
		flash("Successfully logged in "+googleid)
		session['logged_in_id'] = row[0]	   
		session['logged_in_gid'] = googleid	   
		session['logged_in'] = row[1] # 0=a viewing user, 1=an editting user, 2=an admin user
		return True;
	else:
		flash(googleid + " not in local user database ", "error")
		session.pop('logged_in', None)
		session.pop('logged_in_id', None)
		session.pop('logged_in_gid', None)
		return False   

def connect_db():
	"""Connects to the specific database."""
	rv = sqlite3.connect(app.config['DATABASE'])
	rv.row_factory = sqlite3.Row
	return rv

def get_db():
	"""Opens a new database connection if there is none yet for the current application context."""
	if not hasattr(g, 'sqlite_db'):
		g.sqlite_db = connect_db()
	return g.sqlite_db

def init_db():
	"""Initializes the database."""
	db = get_db()
	with app.open_resource('schema.sql', mode='r') as f:
		db.cursor().executescript(f.read())
	db.commit()
	print("Database successfully initialized")
	
def add_user_db(gid, level):
	"""Inserts or updates the rights of the given user in our database."""
	db = get_db()
	cur = db.execute('update users set rights=? where google_id=?', [level, gid])
	if (cur.rowcount == 0):
		cur = db.execute('insert into users (google_id, rights) values(?, ?)', [gid, level])
	db.commit()
	return cur.rowcount

def del_user_db(gid):
	"""Deletes the given user in our database."""
	db = get_db()
	cur = db.execute('delete from users where google_id=?', [gid])
	db.commit()
	return cur.rowcount
	
@app.teardown_appcontext
def teardown_db(exception):
	"""Closes the database again at the end of the request."""
	if hasattr(g, 'sqlite_db'):
		g.sqlite_db.close()
		
	
def render_view_edo_command(userid=None):
	"""Returns a rendering of a basic view page for ED off chain order book"""
	global worker
	return render_template('ed_orders.html', tokens=worker.tokens, params=worker.getParams())

def render_users_command(userid=None):
	"""Returns a rendering of a basic user admin"""
	db = get_db()
	if (userid):
		cur = db.execute('select google_id, rights from users where google_id=?', [userid])
	else:
		cur = db.execute('select google_id, rights from users')
	entries = cur.fetchall()
	return render_template('users.html', title='Administer Users', entries=entries)

@app.cli.command('add')
@click.option('--user', help='a viewing user to add')
@click.option('--admin', help='an admin user to add')
def cli_add2db(user, admin):
	"""Adds a user to the users file either with admin rights or not """
	rows = 0
	if (user):
		rows = add_user_db(user, 0)
		if (rows > 0):
			print ("Successfully inserted user "+user)
	elif (admin):
		rows = add_user_db(admin, 2)
		if (rows > 0):
			print ("Successfully inserted admin "+admin)
	if (rows == 0):
		print ("Nothing to do here")

@app.cli.command('del')
@click.option('--gid', help='id of user to remove')
def cli_del2db(gid):
	"""Removes a user from the users file according to google_id """
	rows = 0
	if (gid):
		rows = del_user_db(gid)
	if (rows > 0):
		print ("Successfully deleted "+gid)
	else:
		print ("Nothing to do here")

@app.cli.command('worker')
def cli_test_worker():
	"""Just run the worker loop and nothing else, waiting for ctl-C."""
	global worker
	worker = JoDolgozo(sweetness=sweetness, mad_log_file=mad_logging)
	print('Starting up the worker...')
	worker.start()
	while True:
		pass

@app.cli.command('initdb')
def cli_initdb():
	"""Creates the basic database tables. Run from site directory by calling 'flask initdb' from the CLI."""
	init_db()
	print('Initialized the database.')


@app.route('/view')
@app.route('/')
def index():
	"""Main page handler"""
	print(request, file=sys.stderr)
	logged_in = session.get('logged_in', 0)
	if (logged_in):
		return render_view_edo_command(session.get('logged_in_gid', None))
	else:
		return render_view_edo_command()

@app.route('/worker/start', methods=['GET', 'POST'])
def worker_start():
	global worker
	if not worker:
		print("building worker ...")
		worker = JoDolgozo(mad_log_file=mad_logging, sweetness=sweetness, sweet_filter=sweet_filter, shit_token_filter=shit_token_filter, wallet=wallet,
						xpressGasPrice_gwei=xpressGasPrice_gwei, lazyGasPrice_gwei=lazyGasPrice_gwei,
						minimumViableSDGain_eth=minimumViableSDGain_eth, maximumViableSDGain_eth=maximumViableSDGain_eth)
	else:
		print("worker already built ...")		
	if not worker.isRunning:
		print("starting worker ...")
		worker.start()
	else:
		print("worker already running ...")				
	return jsonify(server=worker.joStatus())

@app.route('/worker/shutdown', methods=['GET', 'POST'])
def worker_shutdown():
	global worker
	print("shutting down worker, maybe ...")
	if worker:
		worker.stop()
	return jsonify(server=worker.joStatus())

@app.route('/worker/params', methods=['GET', 'POST'])
def worker_params():
	global worker
	param_result = {}
	if request.method == 'GET':
		if worker:
			param_result=worker.setParams(request.args)
	elif request.method == 'POST':
		if worker:
			param_result=worker.setParams(request.form)
	return jsonify(result=param_result)

@app.route('/worker/ping', methods=['GET', 'POST'])
def worker_ping():
	global worker
	return jsonify(server=worker.joStatus())

@app.route('/useradd', methods=['GET', 'POST'])
def useradd():
	print(request, file=sys.stderr)
	logged_in = session.get('logged_in', 0)
	if (logged_in >= 2):
# handle any requests to modify the users here
		return render_users_command()
	else:
		flash("you must login as an admin first", "error")
		return render_template('main.html')
	
@app.route('/users', methods=['GET', 'POST'])
def users():
	print(request, file=sys.stderr)
	logged_in = session.get('logged_in', 0)
	if (logged_in >= 2):
# handle any requests to modify the users here
		return render_users_command()
	else:
		flash("you must login as an admin first", "error")
		return render_template('main.html')
	
@app.route('/logout')
def logout():
	"""Handler for site logout. We should probably also log out from google here"""
	return ""
	logged_in = session.get('logged_in', 0)
	gid = session.get('logged_in_gid', None)
	if (logged_in):
		set_logout()
		if (gid):
			flash("Successfully logged out "+gid)
	return render_template('layout.html')

@app.route('/login')
def login():
	return ""
	if (mock_login):
		flash("Mock login")
		mock_id = "dakyri@gmail.com"
		session['logged_in_id'] = 2	   
		session['logged_in_gid'] = mock_id	 
		session['logged_in'] = 2 # 0=a viewing user, 1=an editting user, 2=an admin user
		return render_view_edo_command(mock_id);
	return redirect(google_login.authorization_url(), 302)


if __name__ == "__main__":
	app.run(host="0.0.0.0", debug=True)

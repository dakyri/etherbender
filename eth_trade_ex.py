'''
Created on Nov 26, 2017

@author: dak
'''
from web3.main import Web3

"""
wrapper to do the actual contract interaction
"""
from mpmath import fraction

import rlp
from ethereum.transactions import Transaction
from ethereum import utils
from web3.utils import encoding
from web3 import HTTPProvider
from web3 import Web3
import json

class User:
	def __init__(self, passphrase):
		self.key = utils.sha3(passphrase)
		self.addr = utils.privtoaddr(self.key)

abiEtherDelta = json.loads('./contracts/etherdelta.json')
abiToken = json.loads('./contracts/token.json')


def setup(web3, config):		
	c = web3.eth.contract(abiEtherDelta).at(config.addressEtherDelta);
	return c


#######################################################################################################
# from the bots python code

addressEtherDelta = '0x8d12A197cB00D4747a1fe03395095ce2A5CC6819' # etherdelta_2's contract address
web3 = Web3(HTTPProvider('https://mainnet.infura.io/Ky03pelFIxoZdAUsr82w'))

def getBlockNumber(self):
	return web3.eth.blockNumber

from web3.utils.normalizers import abi_ens_resolver
from web3.utils.abi import map_abi_data
from eth_utils import add_0x_prefix, remove_0x_prefix
from web3.utils.encoding import hex_encode_abi_type
import hashlib
import random
from web3.exceptions import InvalidAddress
from web3.utils.ens import (
    is_ens_name,
    validate_name_has_address,
)
def abi_ens_resolver(w3, abi_type, val):
	if abi_type == 'address' and is_ens_name(val):
		if w3 is None:
			raise InvalidAddress("Could not look up name, because no web3 connection available")
		elif w3.ens is None:
			raise InvalidAddress("Could not look up name, because ENS is set to None")
		else:
			return (abi_type, validate_name_has_address(w3.ens, val))
	else:
		return (abi_type, val)

# This function is very similar to Web3.soliditySha3() but there is no Web3.soliditySha256() as per November 2017
# It serializes values according to the ABI types defined in abi_types and hashes the result with sha256.
def soliditySha256(self, abi_types, values):
	normalized_values = map_abi_data([abi_ens_resolver(Web3)], abi_types, values)
	#print(normalized_values)
	hex_string = add_0x_prefix(''.join(
		remove_0x_prefix(hex_encode_abi_type(abi_type, value))
		for abi_type, value
		in zip(abi_types, normalized_values)
	))
	#print(hex_string)
	hash_object = hashlib.sha256(Web3.toBytes(hexstr=hex_string))
	return hash_object.hexdigest()


def createOrder(self, side, expires, price, amount, token, userAccount, user_wallet_private_key, randomseed = None):
	global addressEtherDelta, web3

	print("\nCreating '" + side + "' order for %.18f tokens @ %.18f ETH/token" % (amount, price))

	# Validate the input
	if len(user_wallet_private_key) != 64: raise ValueError('WARNING: user_wallet_private_key must be a hexadecimal string of 64 characters long')

	# Ensure good parameters
	token = Web3.toChecksumAddress(token)
	userAccount = Web3.toChecksumAddress(userAccount)
	user_wallet_private_key = Web3.toBytes(hexstr=user_wallet_private_key)

	# Build the order parameters
	amountBigNum = amount
	amountBaseBigNum = float(amount) * float(price)
	if randomseed != None: random.seed(randomseed)	# Seed the random number generator for unit testable results
	orderNonce = random.randint(0,10000000000)
	if side == 'sell':
		tokenGive = token
		tokenGet = '0x0000000000000000000000000000000000000000'
		amountGet = web3.toWei(amountBaseBigNum, 'ether')
		amountGive = web3.toWei(amountBigNum, 'ether')
	elif side == 'buy':
		tokenGive = '0x0000000000000000000000000000000000000000'
		tokenGet = token
		amountGet = web3.toWei(amountBigNum, 'ether')
		amountGive = web3.toWei(amountBaseBigNum, 'ether')
	else:
		print("WARNING: invalid order side, no action taken: " + str(side))

	# Serialize (according to ABI) and sha256 hash the order's parameters
	hashhex = self.soliditySha256(
		['address', 'address', 'uint256', 'address', 'uint256', 'uint256', 'uint256'],
		[addressEtherDelta, tokenGet, amountGet, tokenGive, amountGive, expires, orderNonce]
	)
	# Sign the hash of the order's parameters with our private key (this also addes the "Ethereum Signed Message" header)
	signresult = web3.eth.account.sign(message_hexstr=hashhex, private_key=user_wallet_private_key)
	#print("Result of sign:" + str(signresult))

	orderDict = {
		'amountGet' : amountGet,
		'amountGive' : amountGive,
		'tokenGet' : tokenGet,
		'tokenGive' : tokenGive,
		'contractAddr' : addressEtherDelta,
		'expires' : expires,
		'nonce' : orderNonce,
		'user' : userAccount,
		'v' : signresult['v'],
		'r' : signresult['r'].hex(),
		's' : signresult['s'].hex(),
	}
	return orderDict
 
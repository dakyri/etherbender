'''
Created on Nov 26, 2017

@author: dak
'''

"""
wrapper to do the actual contract interaction
"""
import rlp
from ethereum.transactions import Transaction
from ethereum import utils
from web3.utils import encoding
from web3 import HTTPProvider
from web3 import Web3
import json
from time import sleep
from benttp_provider import BentTPProvider

class User:
	def __init__(self, addr, key):
		self.addr = addr
		self.key = key

# https://bitcoin.stackexchange.com/questions/38351/ecdsa-v-r-s-what-is-v
# This has nothing to do with RFC6979, but with ECDSA signing and public key recovery.
# The (r, s) is the normal output of an ECDSA signature, where r is computed as the X coordinate of a point R, modulo the curve order n.
# In Bitcoin, for message signatures, we use a trick called public key recovery. The fact is that if you have the full R point 
# (not just its X coordinate) and s, and a message, you can compute for which public key this would be a valid signature. What this allows
#  is to 'verify' a message with an address, without needing to know the full key (we just to public key recovery on the signature, and then												
# hash the recovered key and compare it with the address).
# However, this means we need the full R coordinates. There can be up to 4 different points with a given "X coordinate modulo n". 
# (2 because each X coordinate has two possible Y coordinates, and 2 because r+n may still be a valid X coordinate).
# That number between 0 and 3 we call the recovery id, or recid. Therefore, we return an extra byte, which also functions as a header byte,
# by using 27+recid (for uncompressed recovered pubkeys) or 31+recid (for compressed recovered pubkeys).

#	"0xb8c77482e45f1f44de1745f52c74426c631bdd52" "BNB"
	
#	>>> w.web3.eth.getTransactionReceipt('0xa042fb6422437e86e7506173e37bf188f17decb6e5ad649080a59ad357c42e09')
#	AttributeDict({'blockHash': '0xc4c038e8a23442cf483e761f905984e3d3a047bb2365c235ce965f85d44e8123', 'blockNumber': 4802316, 'contractAddress': None, 'cumulativeGasUsed': 4732229, 'from': '0xdcce32ecd82fb651983b10ba53bf3141a330c953', 'gasUsed': 108050, 'logs': [AttributeDict({'address': '0x8d12A197cB00D4747a1fe03395095ce2A5CC6819', 'topics': ['0x6effdda786735d5033bfad5f53e5131abcced9e52be6c507b62d639685fbed6d'], 'data': '0x000000000000000000000000b8c77482e45f1f44de1745f52c74426c631bdd5200000000000000000000000000000000000000000000000000038d7ea4c6800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000082f79cd90000000000000000000000000009102be016ecb172a7a1a45910abb1b1b32d0c84d000000000000000000000000dcce32ecd82fb651983b10ba53bf3141a330c953', 'blockNumber': 4802316, 'transactionHash': '0xa042fb6422437e86e7506173e37bf188f17decb6e5ad649080a59ad357c42e09', 'transactionIndex': 153, 'blockHash': '0xc4c038e8a23442cf483e761f905984e3d3a047bb2365c235ce965f85d44e8123', 'logIndex': 39, 'removed': False})], 'logsBloom': '0x00000000000000000000000000000000000000000000000000000000000000080000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000008000000000000000000000000000000400000000000000000000000000040000000000000000000000000000000000000000000000000000000000000000000000000000000000000080', 'status': 1, 'to': '0x8d12a197cb00d4747a1fe03395095ce2a5cc6819', 'transactionHash': '0xa042fb6422437e86e7506173e37bf188f17decb6e5ad649080a59ad357c42e09', 'transactionIndex': 153})

# anonymous in the ABI:
# topics[0]: keccak(EVENT_NAME+"("+EVENT_ARGS.map(canonical_type_of).join(",")+")") (canonical_type_of is a function that simply returns
# the canonical type of a given argument, e.g. for uint indexed foo, it would return uint256).
# If the event is declared as anonymous the topics[0] is not generated;

# from docs on tokens:
# contract ERC20 {
#     function totalSupply() constant returns (uint totalSupply);
#     function balanceOf(address _owner) constant returns (uint balance);
#     function transfer(address _to, uint _value) returns (bool success);
#     function transferFrom(address _from, address _to, uint _value) returns (bool success);
#     function approve(address _spender, uint _value) returns (bool success);
#     function allowance(address _owner, address _spender) constant returns (uint remaining);
#     event Transfer(address indexed _from, address indexed _to, uint _value);
#     event Approval(address indexed _owner, address indexed _spender, uint _value);
# }
# 	string public constant name = "Token Name";
# 	string public constant symbol = "SYM";
# 	uint8 public constant decimals = 18;  // 18 is the most common number of decimal places

# from solidity contract:
# these functions for depositing and withdrawing ERC20 token amounts
#   function depositToken(address token, uint amount) {
#     //remember to call Token(address).approve(this, amount) or this contract will not be able to do the transfer on your behalf.
#     if (token==0) throw;
#     if (!Token(token).transferFrom(msg.sender, this, amount)) throw;
#     tokens[token][msg.sender] = safeAdd(tokens[token][msg.sender], amount);
#     Deposit(token, msg.sender, amount, tokens[token][msg.sender]);
#   }
# 
#   function withdrawToken(address token, uint amount) {
#     if (token==0) throw;
#     if (tokens[token][msg.sender] < amount) throw;
#     tokens[token][msg.sender] = safeSub(tokens[token][msg.sender], amount);
#     if (!Token(token).transfer(msg.sender, amount)) throw;
#     Withdraw(token, msg.sender, amount, tokens[token][msg.sender]);
#   }
#
# these functions for depositing and withdrawing eth
# function deposit() payable {
#   tokens[0][msg.sender] = safeAdd(tokens[0][msg.sender], msg.value);
#   Deposit(0, msg.sender, msg.value, tokens[0][msg.sender]);
# }
# The deposit function is the only payable function in the entire smart contract. ie the only one with non zero value ????
# 
# function withdraw(uint amount) {
#   if (msg.value>0) throw;
#   if (tokens[0][msg.sender] < amount) throw;
#   tokens[0][msg.sender] = safeSub(tokens[0][msg.sender], amount);
#   if (!msg.sender.call.value(amount)()) throw;
#   Withdraw(0, msg.sender, amount, tokens[0][msg.sender]);
# }


class w3w:

# requests.exceptions.ConnectionError should maybe cause a retry. let all other exceptions fall through


	def getTransactionReceipt(self, txhash):
		'''
		@return: transaction receipt if there is one, and it might look something like this
			AttributeDict({
			 	'blockHash': '0xc4c038e8a23442cf483e761f905984e3d3a047bb2365c235ce965f85d44e8123', 	
			 	'blockNumber': 4802316,
			 	'contractAddress': None, 
			 	'cumulativeGasUsed': 4732229,
			 	'from': '0xdcce32ecd82fb651983b10ba53bf3141a330c953',
			 	'gasUsed': 108050, 
			 	'logs': [
			 		AttributeDict({
			 			'address': '0x8d12A197cB00D4747a1fe03395095ce2A5CC6819',
			 			'topics': ['0x6effdda786735d5033bfad5f53e5131abcced9e52be6c507b62d639685fbed6d'],
			 		 	'data': '0x000000000000000000000000b8c77482e45f1f44de1745f52c74426c631bdd5200000000000000000000000000000000000000000000000000038d7ea4c6800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000082f79cd90000000000000000000000000009102be016ecb172a7a1a45910abb1b1b32d0c84d000000000000000000000000dcce32ecd82fb651983b10ba53bf3141a330c953', 
			 			'blockNumber': 4802316,
			 			'transactionHash': '0xa042fb6422437e86e7506173e37bf188f17decb6e5ad649080a59ad357c42e09', 
			 			'transactionIndex': 153,
			 			'blockHash': '0xc4c038e8a23442cf483e761f905984e3d3a047bb2365c235ce965f85d44e8123',
			 			'logIndex': 39,
			 			'removed': False})],
			 	'logsBloom': '0x00000000000000000000000000000000000000000000000000000000000000080000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000008000000000000000000000000000000400000000000000000000000000040000000000000000000000000000000000000000000000000000000000000000000000000000000000000080',
			 	'status': 1,
			 	'to': '0x8d12a197cb00d4747a1fe03395095ce2a5cc6819',
			 	'transactionHash': '0xa042fb6422437e86e7506173e37bf188f17decb6e5ad649080a59ad357c42e09',
			 	'transactionIndex': 153})
		'''
		return self.web3.eth.getTransaction(txhash)

	def getBlockNumber(self):
		'''
		@return: current block number
		'''
		b = self.web3.eth.blockNumber
		return b
	
	def gasTimate(self, sender, **tradeArgs):
		gas = self.contractEtherDelta.estimateGas().trade(sender=sender, **tradeArgs)
		return gas

	def getEDTokenBalance(self, user, token_addr, unit='wei'):
		'''
		gets the token balance for the given wallet and token address held by ED
		
		@param user: a User structure giving the address of the wallet (hex string starting with 0x...)
		@param token_adr: the address of the token in question (hex string starting with 0x...)
		@param unit: the units for the return value
		@return: the balance as an integer in wei, or as a decimal.Decimal for any other unit
		'''
		b = self.contractEtherDelta.call().balanceOf(user=user.addr, token=token_addr)
		if unit == 'wei':
			return b
		return self.web3.fromWei(b, unit)
	
	def getTokenBalance(self, user, token_addr, unit='wei'):
		'''
		gets the given tokens balance for the given wallet
		
		@param user: a User structure giving the address of the wallet (hex string starting with 0x...)
		@param token_adr: the address of the token in question (hex string starting with 0x...)
		@param unit: the units for the return value
		@return: the balance as an integer in wei, or as a decimal.Decimal for any other unit
		'''
		token = self.web3.eth.contract(
		    token_addr,
		    abi=self.abiToken,
		)
		b = token.call().balanceOf(user.addr)
		if unit == 'wei':
			return b
		return self.web3.fromWei(b, unit)
	
	def getERC20Info(self, token_addr):
		'''
		gets the token info for the token at the contract address
		@param token_addr: a User structure giving the address of the wallet (hex string starting with 0x...)
		@return name: name string
		@return symbol: symbol string
		@return decimal: number of decimals
		'''
		token = self.web3.eth.contract(token_addr, abi=self.abiToken)
		name = token.call().name()
		symbol = token.call().symbol()
		decimals = token.call().decimals()
		return name, symbol, decimals
		
	def getBalance(self, user, unit='wei'):
		'''
		gets the total eth balance for the given wallet
		
		@param user: a User structure giving the address of the wallet (hex string starting with 0x...)
		@param unit: the units for the return value
		@return: the balance as an integer in wei, or as a decimal.Decimal for any other unit
		'''
		b = self.web3.eth.getBalance(user.addr)
		if unit == 'wei':
			return b
		return self.web3.fromWei(b, unit)
	
	def makeTradeArgs(self, order, amount_wei):
		'''
		@return: a dictionary of the appropriate parameters for a trade with ED
		'''
		ta = {
			'tokenGet' : Web3.toChecksumAddress(order.tokenGet),
			'amountGet' : order.amountGet,
			'tokenGive' : Web3.toChecksumAddress(order.tokenGive),
			'amountGive' : order.amountGive,
			'expires' : order.expires,
			'nonce' : order.nonce,
			'user' : Web3.toChecksumAddress(order.user),
			'amount' : int(amount_wei),
		}
		if hasattr(order, 'v'):
			ta['v'] = order.v
		else:
			ta['v'] = 0
		if hasattr(order, 'r'):
			ta['r'] = self.web3.toBytes(hexstr=order.r)
		else:
			ta['r'] = ''
		if hasattr(order, 's'):
			ta['s'] = self.web3.toBytes(hexstr=order.s)
		else:
			ta['s'] = ''			
		return ta

	
	def takeEDOrder(self, user, order, fraction=1, amountGive_tok=0, amountGive_wei=0, gasPrice_gwei=4, do_testTrade=True, do_gasEstimate=True):
		'''
		take an order synchronously, governed by the given fraction/amount
		
		@param user: a User structure giving the wallet address and private key
		@param order: an Order structure giveing the basic order of interest
		@param fraction: relative amount of the amountGet to that we will send from our wallet
		@param amountGive_tok: exact amount our wallet gives in the exchange in units of token (ETH if we are buying token, token if we are selling tokens)
		@param amountGive_wei: exact amount of the order to take in wei
		@param gasPrice_gwei: the gas price we are bidding
		@param do_testTrade: conduct a basic pre-test of the trade params
		@return: the transaction hash if we get that far, and the gas estimate
		'''

		if amountGive_wei == 0:
			amountGive_wei = (order.amountGet * fraction) if amountGive_tok == 0 else self.web3.toWei(amountGive_tok, 'ether') 
			# todo FIXME XXXX need some big num stuff in the fractiony bit
		
		tx = None
		gas = 0

		tradeArgs = self.makeTradeArgs(order, amountGive_wei)
		print(str(tradeArgs))
		if do_gasEstimate:
			gas = self.contractEtherDelta.estimateGas().trade(**tradeArgs)
		if user:
			if do_testTrade:
				test_result = self.contractEtherDelta.call().testTrade(sender=user.addr, **tradeArgs)
				if not test_result:
					raise Exception('bad result from contractED.testTrade')
			self.send(user, self.contractEtherDelta, self.addressEtherDelta, method='trade', methodArgs=tradeArgs, gasPrice_gwei=gasPrice_gwei)
		return tx, gas
	
	def depositEDToken(self, user, token_addr, amount, gasPrice_gwei=4):
		'''
		deposit an token amount in ED.
		!!! BAD BLOCKING SOLUTION !!!!
				
		@param user: a User structure giving the wallet address and private key
		@param token_adr: the address of the token in question (hex string starting with 0x...)
		@return: the transaction hash if we get that far
		'''
		amount_wei = self.web3.toWei(amount, 'ether')
		tokenContract = self.web3.eth.contract(
		    token_addr,
		    abi=self.abiToken,
		)
		approveArgs = { '_sender': self.addressEtherDelta, '_value': amount}
		txhash = self.send(user, tokenContract, token_addr, 'approve', methodArgs=approveArgs, gasPrice_gwei=gasPrice_gwei)
		rx = None
		while True:
			rx = self.getTransactionReceipt(txhash)
			if rx is None:
				sleep(0.1)
			else:
				break
		dedArgs = { 'token': token_addr, 'amount': amount_wei }
		return self.send(user, self.contractEtherDelta, self.addressEtherDelta, 'depositToken', methodArgs=dedArgs, gasPrice_gwei=gasPrice_gwei)
	
	def withdrawEDToken(self, user, token_addr, amount, gasPrice_gwei=4):
		'''
		withdraw a token value from ED
				
		@param user: a User structure giving the wallet address and private key
		@param token_adr: the address of the token in question (hex string starting with 0x...)
		@return: the transaction hash if we get that far
		'''
		amount_wei = self.web3.toWei(amount, 'ether')
		wedArgs = { 'token': token_addr, 'amount': amount_wei }
		return self.send(user, self.contractEtherDelta, self.addressEtherDelta, 'withdrawToken', methodArgs=wedArgs, gasPrice_gwei=gasPrice_gwei)

	def depositEDEth(self, user, amount, gasPrice_gwei=4):
		'''
		deposit an ethereum amount in ED
				
		@param user: a User structure giving the wallet address and private key
		@return: the transaction hash if we get that far
		'''
		amount_wei = self.web3.toWei(amount, 'ether')
		return self.send(user, self.contractEtherDelta, self.addressEtherDelta, 'deposit', value=amount_wei, gasPrice_gwei=gasPrice_gwei)

	def withdrawEDEth(self, user, amount, gasPrice_gwei=4):
		'''
		withdraw an ethereum amount from ED
				
		@param user: a User structure giving the wallet address and private key
		@return: the transaction hash if we get that far
		'''
		amount_wei = self.web3.toWei(amount, 'ether')
		wedArgs = { 'amount': amount_wei }
		return self.send(user, self.contractEtherDelta, self.addressEtherDelta, 'withdraw', methodArgs=wedArgs, gasPrice_gwei=gasPrice_gwei)

	def send(self, user, contract, destination, method, methodArgs={}, gasPrice_gwei=4, value=0):
		'''
		send a transaction
				
		@param user: a User structure giving the wallet address and private key
		@param contract: a web3.eth initialized object for the contract we're talking with
		@param address: string. the destination address for our transaction with that contract
		@param method: string. the method name
		@param methodArgs: dictionary. arguments to that method
		@param gasPrice_gwei: int. our bid in gwei
		@param value: int. the value we're sending
		'''
		nonce=0
		try:
			nonce = self.web3.eth.getTransactionCount(user.addr)
		except Exception as e:
			raise Exception('bad nonce from getTransactionCount, '+e.message)

		gasPrice_wei = self.web3.toWei(gasPrice_gwei, 'gwei')
		
		abidata_hex = contract.encodeABI(method, kwargs=methodArgs)
		abidata = bytes(bytearray.fromhex(abidata_hex[2:]))
		
		tx = Transaction(
	    		nonce=nonce,				# nonce from getTransactionAccount above
	    		gasprice=gasPrice_wei,		# gasprice
	    		startgas=self.gasLimit,		# startgas
	    		to=destination,				# to	
				value=value,				# value=0 for most ED operations
				data=abidata,				# data  ... plus optionally  v=0, r=0, s=0
			)
		tx.sign(user.key)
		raw_tx = rlp.encode(tx)
		raw_tx_hex = encoding.to_hex(raw_tx) # web3.toHex ... I think these are more or less the same
		result = self.web3.eth.sendRawTransaction(raw_tx_hex)
		return result

	def __init__(self,
				web3=None,
				httpProvider='https://mainnet.infura.io/Ky03pelFIxoZdAUsr82w',
				edAddr='0x8d12A197cB00D4747a1fe03395095ce2A5CC6819', # etherdelta_2's contract address
				abiED='./contracts/etherdelta.json',
				abiTok='./contracts/token.json',
				gasLimit=250000):
		self.abiEtherDelta = None
		self.abiToken = None
		with open(abiED, 'r') as abi_fp:
			self.abiEtherDelta = json.load(abi_fp)
		with open(abiTok, 'r') as abi_fp:
			self.abiToken = json.load(abi_fp)
		self.gasLimit = gasLimit
		
		if web3 is None:
			provider = None
			self.web3 = None
			if httpProvider:
				provider = BentTPProvider(httpProvider)
			if provider:
				self.web3 = Web3(provider)	
		else:
			self.web3 = web3

		self.addressEtherDelta = Web3.toChecksumAddress(edAddr)
		self.contractEtherDelta = self.web3.eth.contract(address=self.addressEtherDelta, abi=self.abiEtherDelta)
